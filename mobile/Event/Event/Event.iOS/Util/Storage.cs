﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using System.IO;
using Event.Util;
using Event.iOS.Util;
using System.Net;
using CoreFoundation;
using SystemConfiguration;
using Xamarin.Forms;
using Event.Services;

[assembly: Dependency(typeof(Storage))]
namespace Event.iOS.Util
{
    public class Storage : IStorage
    {
        public string GetStorage(string NomeArquivoBanco)
        {
            string caminhoDaPasta = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            string caminhoDaBiblioteca = Path.Combine(caminhoDaPasta, "..", "Library");
            string caminhoBanco = Path.Combine(caminhoDaBiblioteca, NomeArquivoBanco);
            return caminhoBanco;
        }
    }
}
