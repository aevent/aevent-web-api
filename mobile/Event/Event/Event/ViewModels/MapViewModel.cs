﻿using Event.Models;
using Event.Services;
using Event.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Event.ViewModels
{
    public class MapViewModel : BaseViewModel
    {
        public ObservableCollection<Empresa> Empresas { get; set; }
        public Command LoadEmpresasCommand { get; set; }
        public Command LoadInfoCommand { get; set; }
        public MapViewModel()
        {
            Title = "Sobre o Evento";
            Empresas = new ObservableCollection<Empresa>();
            LoadEmpresasCommand = new Command(async () => await ExecuteLoadEmpresasCommandAsync());
            LoadInfoCommand = new Command(async () => await ExecuteLoadEventoAsync());
        }

        async Task ExecuteLoadEventoAsync()
        {
            try
            {
                List<Usuario> users = BankConnection.Consultar<Usuario>();

                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("id", users[0].EventoId.ToString());
                string result = await ConnectionUtil.performPostCallAsync("listarEventoPorId", parameters);

                Evento evento = JsonConvert.DeserializeObject<Evento>(result);

                Imagem = evento.Mapa;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        async Task ExecuteLoadEmpresasCommandAsync()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            try
            {
                Empresas.Clear();
                string result = await ConnectionUtil.performGetCallAsync("listarEmpresas");

                List<Empresa> empresas = JsonConvert.DeserializeObject<List<Empresa>>(result);
                foreach (var empresa in empresas)
                {
                    Empresas.Add(empresa);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        private string _Imagem;
        public string Imagem
        {
            get { return _Imagem; }
            set { SetProperty(ref _Imagem, value); }
        }
    }
}
