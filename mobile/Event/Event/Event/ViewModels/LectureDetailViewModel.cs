﻿using Event.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Event.ViewModels
{
    public class LectureDetailViewModel : BaseViewModel
    {
        public Palestra Palestra { get; set; }
        public LectureDetailViewModel(Palestra palestra = null)
        {
            Title = palestra?.NomePalestra;
            Palestra = palestra;
        }
    }
}
