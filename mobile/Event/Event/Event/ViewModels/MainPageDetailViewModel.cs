﻿using Event.Models;
using Event.Services;
using Event.Util;
using Event.Views;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using ZXing.Net.Mobile.Forms;

namespace Event.ViewModels
{
    public class MainPageDetailViewModel : BaseViewModel
    {
        ZXingScannerPage scanPage;
        INavigation navigation;
        public ObservableCollection<Empresa> Empresas { get; set; }
        public Command LoadEmpresasCommand { get; set; }
        public Command LoadUsuariosCommand { get; set; }
        public Command ScanCommand { get; set; }
        public Command MapCommand { get; set; }
        public Command AddCommand { get; set; }
        public Usuario user;
        public ObservableCollection<Usuario> Usuarios { get; set; }
        public string NomeEmpresa { get; set; }

        public MainPageDetailViewModel(INavigation _navigation)
        {
            user = BankConnection.Consultar<Usuario>().First();
            navigation = _navigation;
            if (user.UsuarioQR != null)
            {
                Empresas = new ObservableCollection<Empresa>();
                UserListVisible = false;
                EmpresaListVisible = true;
            }
            else
            {
                Usuarios = new ObservableCollection<Usuario>();
                UserListVisible = true;
                EmpresaListVisible = false;
                NomeEmpresa = user.Empresa.ToUpper();
            }
            
            LoadEmpresasCommand = new Command(async () => await ExecuteLoadEmpresasCommandAsync());
            LoadUsuariosCommand = new Command(async () => await ExecuteLoadUsuariosCommandAsync());
            ScanCommand = new Command(async () => await ExecuteScanCommandAsync());
            MapCommand = new Command(async () => await ExecuteMapCommandAsync());
            AddCommand = new Command(async () => await ExecuteAddCommandAsync());
        }

        public async Task<bool> adicionarVisita(string UsuarioQR)
        {
            if (UsuarioQR == "" || UsuarioQR == null)
            {
                return false;
            }
            try
            {
                Dictionary<string, string> parameter = new Dictionary<string, string>();
                parameter.Add("empresaid", user.EmpresaId.ToString());
                parameter.Add("eventoid", user.EventoId.ToString());
                parameter.Add("usuarioqr", UsuarioQR);

                string result = await ConnectionUtil.performPostCallAsync("adicionarVisita", parameter);

                double numero = 0;
                return double.TryParse(result,out numero);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                return false;
            }
        }

        async Task ExecuteLoadEmpresasCommandAsync()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            try
            {
                Empresas.Clear();
                
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("userid", user.Id.ToString());
                parameters.Add("eventoid", user.EventoId.ToString());
                
                string result = await ConnectionUtil.performPostCallAsync("listarEmpresasVisitadas", parameters);

                List<Empresa> empresas = JsonConvert.DeserializeObject<List<Empresa>>(result);
                foreach (var empresa in empresas)
                {
                    Empresas.Add(empresa);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        async Task ExecuteLoadUsuariosCommandAsync()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            try
            {
                Usuarios.Clear();
                
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("empresaid", user.EmpresaId.ToString());
                parameters.Add("eventoid", user.EventoId.ToString());

                string result = await ConnectionUtil.performPostCallAsync("listarUsuariosQueVisitou", parameters);

                List<Usuario> usuarios = JsonConvert.DeserializeObject<List<Usuario>>(result);
                foreach (var usuario in usuarios)
                {
                    Usuarios.Add(usuario);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        async Task ExecuteMapCommandAsync()
        {
            await navigation.PushAsync(new MapPage());
        }
        async Task ExecuteScanCommandAsync()
        {
            if (user.UsuarioQR != null)
            {
                //se for visitante
                await navigation.PushAsync(new BarcodePage(user.UsuarioQR));
            }
            else
            {
                //se for expositor
                scanPage = new ZXingScannerPage();
                scanPage.OnScanResult += (result) => {
                    scanPage.IsScanning = false;
                    Device.BeginInvokeOnMainThread(async () => {
                        await navigation.PopAsync();
                        if (await adicionarVisita(result.Text.ToString()))
                        {
                            await DisplayAlert("Sucesso!", "Visitante validado com sucesso!");
                        }
                        else
                        {
                            await DisplayAlert("Atenção!", "Não foi possivel validar visitante, repita o procedimento ou tente manualmente.");
                        }
                    });
                };
                await navigation.PushAsync(scanPage);
            }
        }

        async Task ExecuteAddCommandAsync()
        {
            if (UserListVisible)
            {
                await navigation.PushModalAsync(new AddPage(this));
            }
            else
            {
                await DisplayAlert("Atenção","Funcionalidade disponível apenas para expositores");
            }
        }

        public async Task DisplayAlert(string title, string message)
        {
            await Application.Current.MainPage.DisplayAlert(title, message, "Ok");
        }

        private Empresa _EmpresaSelected;
        public Empresa EmpresaSelected
        {
            get { return _EmpresaSelected; }
            set { SetProperty(ref _EmpresaSelected, value); }
        }
        private bool _EmpresaListVisible = true;
        public bool EmpresaListVisible
        {
            get { return _EmpresaListVisible; }
            set { SetProperty(ref _EmpresaListVisible, value); }
        }
        private bool _UserListVisible = false;
        public bool UserListVisible
        {
            get { return _UserListVisible; }
            set { SetProperty(ref _UserListVisible, value); }
        }
    }
}
