﻿using Event.Models;
using Event.Services;
using Event.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;

using Xamarin.Forms;

namespace Event.ViewModels
{
    public class AboutViewModel : BaseViewModel
    {
        public Command LoadInfoCommand { get; set; }
        public AboutViewModel()
        {
            Title = "Sobre o Evento";
            LoadInfoCommand = new Command(async () => await ExecuteLoadEventoAsync());
        }

        async Task ExecuteLoadEventoAsync()
        {
            try
            {
                List<Usuario> users = BankConnection.Consultar<Usuario>();
                
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("id",users[0].EventoId.ToString());
                string result = await ConnectionUtil.performPostCallAsync("listarEventoPorId",parameters);

                Evento evento = JsonConvert.DeserializeObject<Evento>(result);

                Nome = evento.Nome;
                Local = evento.Local;
                Descricao = evento.Descricao;
                Banner = evento.Banner;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        private string _Nome;
        public string Nome
        {
            get { return _Nome; }
            set { SetProperty(ref _Nome, value); }
        }
        private string _Local;
        public string Local
        {
            get { return _Local; }
            set { SetProperty(ref _Local, value); }
        }
        private string _Descricao;
        public string Descricao
        {
            get { return _Descricao; }
            set { SetProperty(ref _Descricao, value); }
        }
        private string _Banner;
        public string Banner
        {
            get { return _Banner; }
            set { SetProperty(ref _Banner, value); }
        }
    }
}