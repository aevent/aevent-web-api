﻿using Event.Models;
using Event.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Event.ViewModels
{
    public class LectureViewModel : BaseViewModel
    {
        public ObservableCollection<Palestra> Palestras { get; set; }
        public Command LoadPalestrasCommand { get; set; }

        public LectureViewModel()
        {
            Palestras = new ObservableCollection<Palestra>();
            LoadPalestrasCommand = new Command(async () => await ExecuteLoadPalestrasCommandAsync());
        }

        async Task ExecuteLoadPalestrasCommandAsync()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            try
            {
                Palestras.Clear();
                string result = await ConnectionUtil.performGetCallAsync("listarPalestras");

                List<Palestra> palestras = JsonConvert.DeserializeObject<List<Palestra>>(result);
                foreach (var palestra in palestras)
                {
                    Palestras.Add(palestra);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
