﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Event.Models
{
    public class Palestra
    {
        public int Id { get; set; }
        public string NomePalestra { get; set; }
        public string NomePalestrante { get; set; }
        public string Descricao { get; set; }
        public string Data { get; set; }
        public string Hora { get; set; }
        public string LocalPalestra { get; set; }
    }
}
