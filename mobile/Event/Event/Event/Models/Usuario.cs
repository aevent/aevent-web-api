﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Event.Models
{
    public class Usuario
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public int? RoleId { get; set; }
        public string Role { get; set; }
        public int? EmpresaId { get; set; }
        public string Empresa { get; set; }
        public string UsuarioQR { get; set; }
        public int? EventoId { get; set; }
        public string Evento { get; set; }
    }
}
