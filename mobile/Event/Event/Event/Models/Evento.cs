﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Event.Models
{
    public class Evento
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Local { get; set; }
        public string Descricao { get; set; }
        public string Mapa { get; set; }
        public string Banner { get; set; }

        public override string ToString()
        {
            return Nome;
        }
    }
}
