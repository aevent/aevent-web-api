﻿using Event.Models;
using Event.ViewModels;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Event.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LecturePage : ContentPage
    {
        public ObservableCollection<string> Items { get; set; }
        LectureViewModel vm;

        public LecturePage()
        {
            BindingContext = vm = new LectureViewModel();
            InitializeComponent();
        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var item = args.SelectedItem as Palestra;
            if (item == null)
                return;

            await Navigation.PushAsync(new LectureDetailPage(new LectureDetailViewModel(item)));

            // Manually deselect item
            ItemsListView.SelectedItem = null;
        }

        protected override async void OnAppearing()
        {
            vm.LoadPalestrasCommand.Execute(null);
        }
    }
}
