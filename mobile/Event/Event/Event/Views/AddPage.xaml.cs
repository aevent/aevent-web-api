﻿using Event.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Event.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AddPage : ContentPage
	{
        MainPageDetailViewModel vm;
        public AddPage (MainPageDetailViewModel mainPageDetailViewModel)
		{
			InitializeComponent ();
            vm = mainPageDetailViewModel;
		}

        private async void Button_Clicked(object sender, EventArgs e)
        {
            if(await vm.adicionarVisita(entryvalue.Text))
            {
                await DisplayAlert("Sucesso!", "Visitante validado com sucesso!","Ok");
            }
            else
            {
                await DisplayAlert("Atenção!", "Não foi possivel validar visitante, repita o procedimento ou tente manualmente.", "Ok");
            }
            await Navigation.PopModalAsync();

        }
    }
}