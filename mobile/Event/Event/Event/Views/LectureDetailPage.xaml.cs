﻿using Event.Models;
using Event.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Event.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LectureDetailPage : ContentPage
	{
        LectureDetailViewModel viewModel;
        // Note - The Xamarin.Forms Previewer requires a default, parameterless constructor to render a page.
        public LectureDetailPage()
        {
            InitializeComponent();
            
            var item = new Palestra();
            viewModel = new LectureDetailViewModel(item);
            BindingContext = viewModel;
        }

        public LectureDetailPage(LectureDetailViewModel viewModel)
        {
            InitializeComponent();

            BindingContext = this.viewModel = viewModel;
        }
    }
}