﻿using Event.Models;
using Event.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Event.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : MasterDetailPage
    {
        Usuario user;
        public MainPage(Usuario usuario)
        {
            InitializeComponent();
            user = usuario;
            //Detail = new NavigationPage(new MainDetailPage());
            MasterPage.ListView.ItemSelected += ListView_ItemSelected;
        }

        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as MainPageMenuItem;
            if (item == null)
                return;

            try
            {
                var page = (Page)Activator.CreateInstance(item.TargetType);
                page.Title = item.Title;
                if (page.Title != "Sair")
                {
                    //Detail.Navigation.PushAsync(page);
                    //Detail = page;
                    Detail = new NavigationPage(page);
                    IsPresented = false;
                }
                else
                {
                    BankConnection.ExcluirTodos<Usuario>();
                    Application.Current.MainPage = new LoginPage();
                }
                //Application.Current.MainPage.Navigation.PushAsync(new MainPage());

                MasterPage.ListView.SelectedItem = null;
            }
            catch (Exception ex)
            {
                //TODO erro generico
                Debug.WriteLine(ex.ToString());
            }


        }
    }
}