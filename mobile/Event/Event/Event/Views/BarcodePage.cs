﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using ZXing.Net.Mobile.Forms;

namespace Event.Views
{
    public class BarcodePage : ContentPage
    {
        ZXingBarcodeImageView barcode;

        public BarcodePage(string value)
        {
            barcode = new ZXingBarcodeImageView
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                AutomationId = "zxingBarcodeImageView",
            };
            barcode.BarcodeFormat = ZXing.BarcodeFormat.QR_CODE;
            barcode.BarcodeOptions.Width = 400;
            barcode.BarcodeOptions.Height = 400;
            // Workaround for iOS
            barcode.WidthRequest = 400;
            barcode.HeightRequest = 400;
            barcode.BarcodeOptions.Margin = 10;
            barcode.BarcodeValue = value;

            Label lbl = new Label
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                Text = value,
                TextColor = Color.FromHex("#00214d"),
                FontSize = 22,
                FontAttributes = FontAttributes.Bold
            };

            StackLayout layout = new StackLayout
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
                BackgroundColor = Color.White
            };

            layout.Children.Add(barcode);
            layout.Children.Add(lbl);
            
            
            Content = layout;
        }
    }
}
