﻿using Event.ViewModels;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Event.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MainPageDetail : ContentPage
	{
        MainPageDetailViewModel vm;

        public MainPageDetail ()
		{
            BindingContext = vm = new MainPageDetailViewModel(Navigation);
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            vm.LoadEmpresasCommand.Execute(null);
            vm.LoadUsuariosCommand.Execute(null);
        }
    }
}