﻿using Event.ViewModels;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Event.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AboutPage : ContentPage
    {
        public AboutViewModel vm;
        public AboutPage()
        {
            BindingContext = vm = new AboutViewModel();
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            vm.LoadInfoCommand.Execute(null);
        }
    }
}