﻿using Event.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Event.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MapPage : ContentPage
	{
        public MapViewModel vm;
        public MapPage()
        {
            BindingContext = vm = new MapViewModel();
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            vm.LoadInfoCommand.Execute(null);
            vm.LoadEmpresasCommand.Execute(null);
        }
    }
}