﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace Event.Services
{
    class BankConnection
    {
        private static SQLiteConnection _conexao;

        public BankConnection()
        {
            var dep = DependencyService.Get<IStorage>();
            string caminho = dep.GetStorage("database.sqlite");
            _conexao = new SQLiteConnection(caminho);
        }

        public static void CriarTabela<T>()
        {
            var dep = DependencyService.Get<IStorage>();
            string caminho = dep.GetStorage("database.sqlite");
            _conexao = new SQLiteConnection(caminho);
            _conexao.CreateTable<T>();
        }

        public static List<T> Consultar<T>() where T : new()
        {
            var dep = DependencyService.Get<IStorage>();
            string caminho = dep.GetStorage("database.sqlite");
            _conexao = new SQLiteConnection(caminho);
            return _conexao.Table<T>().ToList();
        }

        public static T Consultar<T>(T item) where T : new()
        {
            var dep = DependencyService.Get<IStorage>();
            string caminho = dep.GetStorage("database.sqlite");
            _conexao = new SQLiteConnection(caminho);
            return _conexao.Find<T>(item);
        }

        public static int Atualizar<T>(T item)
        {
            var dep = DependencyService.Get<IStorage>();
            string caminho = dep.GetStorage("database.sqlite");
            _conexao = new SQLiteConnection(caminho);
            return _conexao.Update(item);
        }

        public static int Salvar<T>(T item)
        {
            var dep = DependencyService.Get<IStorage>();
            string caminho = dep.GetStorage("database.sqlite");
            _conexao = new SQLiteConnection(caminho);

            return _conexao.Insert(item);
        }

        public static int SalvarTodos<T>(List<T> item)
        {
            var dep = DependencyService.Get<IStorage>();
            string caminho = dep.GetStorage("database.sqlite");
            _conexao = new SQLiteConnection(caminho);
            int result = 0;
            foreach (T t in item)
            {
                result = _conexao.InsertOrReplace(t);
            }
            return result;
        }

        public static int Excluir<T>(T item)
        {
            var dep = DependencyService.Get<IStorage>();
            string caminho = dep.GetStorage("database.sqlite");
            _conexao = new SQLiteConnection(caminho);
            return _conexao.Delete(item);
        }

        public static int ExcluirTodos<T>()
        {
            var dep = DependencyService.Get<IStorage>();
            string caminho = dep.GetStorage("database.sqlite");
            _conexao = new SQLiteConnection(caminho);
            return _conexao.DeleteAll<T>();
        }
    }
}
