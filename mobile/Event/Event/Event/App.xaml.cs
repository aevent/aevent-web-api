﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Event.Views;
using Event.Services;
using Event.Models;
using System.Collections.Generic;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Event
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();

            BankConnection.CriarTabela<Usuario>();

            List<Usuario> users = BankConnection.Consultar<Usuario>();

            if (users.Count > 0)
            {
                MainPage = new MainPage(users[0]);
            }
            else
            {
                MainPage = new LoginPage();
            }
            //MainPage = new LoginPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
