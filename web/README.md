# Admin Codeigniter 3.2.1 

Download the code from repository. Unzip the zip file. 

Open browser; goto localhost/phpmyadmin. 
Import the file "db.sql" in that database. 

Change database.php in application/config/database.php with your own database details that created previously by importing db.sql

Copy the remaining code into your root directory Open browser; goto localhost/[project-folder-name] and press enter

The login screen will appear. 
## To login, 
I am going to provide the user-email ids and password below. 

### System Administrator Account : 
  * email : admin@bewithdhanu.in 
  * password : 123456 

### Manager Account : 
  * email : manager@bewithdhanu.in 
  * password : 123456 

### Employee Account 
  * email : employee@bewithdhanu.in 
  * password : 123456 

Once you logged in with System Administrator account, you can create user or edit previous user if you want.
