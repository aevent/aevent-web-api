/*
 Navicat Premium Data Transfer

 Source Server         : Localhost - Php - Mysql default
 Source Server Type    : MySQL
 Source Server Version : 50723
 Source Host           : localhost:3306
 Source Schema         : aevent

 Target Server Type    : MySQL
 Target Server Version : 50723
 File Encoding         : 65001

 Date: 20/02/2019 01:05:26
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tbl_empresa
-- ----------------------------
DROP TABLE IF EXISTS `tbl_empresa`;
CREATE TABLE `tbl_empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `cnpj` varchar(15) NOT NULL,
  `cidade` varchar(50) NOT NULL,
  `estado` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `cep` varchar(10) NOT NULL,
  `endereco` varchar(100) NOT NULL,
  `razaosocial` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_empresa
-- ----------------------------
BEGIN;
INSERT INTO `tbl_empresa` VALUES (1, 'Empresa 1', '12345678901234', 'Salvador', 'Bahia', 'teste@teste.com', '41300000', 'rua a', 'teste ltda');
COMMIT;

-- ----------------------------
-- Table structure for tbl_evento
-- ----------------------------
DROP TABLE IF EXISTS `tbl_evento`;
CREATE TABLE `tbl_evento` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `local` varchar(100) NOT NULL,
  `evento_status` varchar(2) NOT NULL DEFAULT 'A',
  `evento_dt_criacao` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_evento
-- ----------------------------
BEGIN;
INSERT INTO `tbl_evento` VALUES (2, 'DEVFEST Sao Paulo', 'SENAI', 'A', '2019-02-20 02:24:27');
INSERT INTO `tbl_evento` VALUES (3, 'CARTEIRA', 'EVENTO EM SÃO PAULO', 'A', '2019-02-20 01:17:09');
INSERT INTO `tbl_evento` VALUES (4, '6º ENCONTRO DE NEGÓCIOS NEWIT', 'SÃO PAULO', 'A', '2019-02-20 02:24:27');
INSERT INTO `tbl_evento` VALUES (5, 'EVENTO TESTE', 'SÃO PAULO', 'A', '2019-02-20 01:25:21');
INSERT INTO `tbl_evento` VALUES (7, 'JOÃO EVENTÃÓ', 'BAHIA', 'A', NULL);
COMMIT;

-- ----------------------------
-- Table structure for tbl_items
-- ----------------------------
DROP TABLE IF EXISTS `tbl_items`;
CREATE TABLE `tbl_items` (
  `itemId` int(11) NOT NULL AUTO_INCREMENT,
  `itemHeader` varchar(512) NOT NULL COMMENT 'Heading',
  `itemSub` varchar(1021) NOT NULL COMMENT 'sub heading',
  `itemDesc` text COMMENT 'content or description',
  `itemImage` varchar(80) DEFAULT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDtm` datetime NOT NULL,
  `updatedDtm` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`itemId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_items
-- ----------------------------
BEGIN;
INSERT INTO `tbl_items` VALUES (1, 'jquery.validation.js', 'Contribution towards jquery.validation.js', 'jquery.validation.js is the client side javascript validation library authored by Jörn Zaefferer hosted on github for us and we are trying to contribute to it. Working on localization now', 'validation.png', 0, 1, '2015-09-02 00:00:00', NULL, NULL);
INSERT INTO `tbl_items` VALUES (2, 'CodeIgniter User Management', 'Demo for user management system', 'This the demo of User Management System (Admin Panel) using CodeIgniter PHP MVC Framework and AdminLTE bootstrap theme. You can download the code from the repository or forked it to contribute. Usage and installation instructions are provided in ReadMe.MD', 'cias.png', 0, 1, '2015-09-02 00:00:00', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for tbl_reset_password
-- ----------------------------
DROP TABLE IF EXISTS `tbl_reset_password`;
CREATE TABLE `tbl_reset_password` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) NOT NULL,
  `activation_id` varchar(32) NOT NULL,
  `agent` varchar(512) NOT NULL,
  `client_ip` varchar(32) NOT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `createdBy` bigint(20) NOT NULL DEFAULT '1',
  `createdDtm` datetime NOT NULL,
  `updatedBy` bigint(20) DEFAULT NULL,
  `updatedDtm` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tbl_roles
-- ----------------------------
DROP TABLE IF EXISTS `tbl_roles`;
CREATE TABLE `tbl_roles` (
  `roleId` tinyint(4) NOT NULL AUTO_INCREMENT COMMENT 'role id',
  `role` varchar(50) NOT NULL COMMENT 'role text',
  PRIMARY KEY (`roleId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_roles
-- ----------------------------
BEGIN;
INSERT INTO `tbl_roles` VALUES (1, 'Administrator');
INSERT INTO `tbl_roles` VALUES (2, 'Expositor');
INSERT INTO `tbl_roles` VALUES (3, 'Vistante');
COMMIT;

-- ----------------------------
-- Table structure for tbl_users
-- ----------------------------
DROP TABLE IF EXISTS `tbl_users`;
CREATE TABLE `tbl_users` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) NOT NULL COMMENT 'login email',
  `password` varchar(128) NOT NULL COMMENT 'hashed login password',
  `name` varchar(128) DEFAULT NULL COMMENT 'full name of user',
  `mobile` varchar(20) DEFAULT NULL,
  `roleId` tinyint(4) NOT NULL,
  `empresaId` int(11) DEFAULT NULL,
  `eventoId` int(11) DEFAULT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDtm` datetime NOT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedDtm` datetime DEFAULT NULL,
  PRIMARY KEY (`userId`),
  KEY `fk_empresa` (`empresaId`),
  KEY `fk_evento` (`eventoId`),
  CONSTRAINT `fk_empresa` FOREIGN KEY (`empresaId`) REFERENCES `tbl_empresa` (`id`),
  CONSTRAINT `fk_evento` FOREIGN KEY (`eventoId`) REFERENCES `tbl_evento` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_users
-- ----------------------------
BEGIN;
INSERT INTO `tbl_users` VALUES (1, 'admin@bewithdhanu.in', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'System Administrator', '9890098900', 1, NULL, NULL, 0, 0, '2015-07-01 18:56:49', 1, '2017-06-19 09:22:53');
INSERT INTO `tbl_users` VALUES (2, 'manager@bewithdhanu.in', '$2y$10$Gkl9ILEdGNoTIV9w/xpf3.mSKs0LB1jkvvPKK7K0PSYDsQY7GE9JK', 'Manager', '9890098900', 2, 1, NULL, 0, 1, '2016-12-09 17:49:56', 1, '2019-01-08 20:26:29');
INSERT INTO `tbl_users` VALUES (3, 'employee@bewithdhanu.in', '$2y$10$MB5NIu8i28XtMCnuExyFB.Ao1OXSteNpCiZSiaMSRPQx1F1WLRId2', 'Employee', '9890098900', 3, NULL, 2, 0, 1, '2016-12-09 17:50:22', 1, '2017-06-19 09:23:21');
INSERT INTO `tbl_users` VALUES (4, 'julianamcampos0@gmail.com', '$2y$10$Xa0Dp8G.SPBWazyxZffgo.YrUFSm/1lc7GDDLR1ykHjlMTYZQ.1Qy', 'Juliana', '1988607536', 3, NULL, 2, 1, 1, '2019-01-09 20:31:00', 1, '2019-01-16 16:15:51');
COMMIT;

-- ----------------------------
-- Table structure for tbl_users_evento_empresa
-- ----------------------------
DROP TABLE IF EXISTS `tbl_users_evento_empresa`;
CREATE TABLE `tbl_users_evento_empresa` (
  `id` int(11) NOT NULL,
  `usuarioId` int(11) DEFAULT NULL,
  `eventoId` int(11) DEFAULT NULL,
  `empresaId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresa_fk` (`empresaId`),
  KEY `usuario_fk` (`usuarioId`),
  KEY `evento_fk` (`eventoId`),
  CONSTRAINT `empresa_fk` FOREIGN KEY (`empresaId`) REFERENCES `tbl_empresa` (`id`),
  CONSTRAINT `evento_fk` FOREIGN KEY (`eventoId`) REFERENCES `tbl_evento` (`id`),
  CONSTRAINT `usuario_fk` FOREIGN KEY (`usuarioId`) REFERENCES `tbl_users` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
