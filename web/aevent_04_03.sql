/*
 Navicat MySQL Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 100137
 Source Host           : localhost:3306
 Source Schema         : aevent

 Target Server Type    : MySQL
 Target Server Version : 100137
 File Encoding         : 65001

 Date: 04/03/2019 15:19:39
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tbl_empresa
-- ----------------------------
DROP TABLE IF EXISTS `tbl_empresa`;
CREATE TABLE `tbl_empresa`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_evento` int(11) NOT NULL,
  `id_user` int(11) NULL DEFAULT NULL,
  `qr_code` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nome` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `cnpj` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cidade` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `estado` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cep` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `endereco` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `razaosocial` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `estatus` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'A',
  `dt_criacao` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `empresa_fk_evento`(`id_evento`) USING BTREE,
  INDEX `empresa_fk_user`(`id_user`) USING BTREE,
  CONSTRAINT `empresa_fk_evento` FOREIGN KEY (`id_evento`) REFERENCES `tbl_evento` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `empresa_fk_user` FOREIGN KEY (`id_user`) REFERENCES `tbl_users` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 113 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbl_empresa
-- ----------------------------
INSERT INTO `tbl_empresa` VALUES (16, 4, 746, 'PeH9AbKu', '01 - NEWIT', '', '', '', '', '', NULL, 'NEWIT', 'A', '2019-02-28 17:13:46');
INSERT INTO `tbl_empresa` VALUES (17, 4, 805, 'zFbzqjbX', '02 - ARAM HOTÉIS', '', '', '', '', '', NULL, 'ARAM HOTÉIS', 'A', '2019-02-28 16:49:53');
INSERT INTO `tbl_empresa` VALUES (18, 4, 762, 'inhdOkqM', '03 - VIALE HOTÉIS', '', '', '', '', '', NULL, 'VIALE HOTÉIS', 'A', '2019-02-28 16:50:26');
INSERT INTO `tbl_empresa` VALUES (19, 4, 750, 'Iawuw0CO', '04 - FIESTA BAHIA HOTEL', '', '', '', '', '', NULL, 'FIESTA BAHIA HOTEL', 'A', '2019-02-28 16:51:04');
INSERT INTO `tbl_empresa` VALUES (20, 4, NULL, '9xbHY0IC', '05 - MACEIÓ MAR', NULL, NULL, NULL, NULL, NULL, NULL, 'MACEIÓ MAR', 'A', '2019-02-24 21:50:33');
INSERT INTO `tbl_empresa` VALUES (21, 4, NULL, 'TCZ6xWPX', '06 - ACQUA SUITE', '', '', '', '', '', NULL, 'ACQUA SUITE', 'A', '2019-02-24 21:49:47');
INSERT INTO `tbl_empresa` VALUES (22, 4, 761, 'lO1oQgdt', '06 - BROCKER TURISMO', '', '', '', '', '', NULL, 'BROCKER TURISMO', 'A', '2019-02-28 17:16:34');
INSERT INTO `tbl_empresa` VALUES (23, 4, 728, 'mImMeaCd', '06 - SNOWLAND', '', '', '', '', '', NULL, 'SNOWLAND', 'A', '2019-02-28 17:15:27');
INSERT INTO `tbl_empresa` VALUES (24, 4, 718, 'M9UTWC3q', '06 - WYNDHAM GRAMADO THERMAS RESORT & SPA', '', '', '', '', '', NULL, 'WYNDHAM GRAMADO THERMAS RESORT & SPA', 'A', '2019-02-28 17:20:32');
INSERT INTO `tbl_empresa` VALUES (25, 4, 774, 'SHYlbePd', '07 - ALL BRAZIL TOURS', '', '', '', '', '', NULL, 'ALL BRAZIL TOURS', 'A', '2019-02-28 16:54:43');
INSERT INTO `tbl_empresa` VALUES (26, 4, NULL, 'SJjpZCQe', '08 - BUZIOS BEACH RESORT', NULL, NULL, NULL, NULL, NULL, NULL, 'BUZIOS BEACH RESORT', 'A', '2019-02-24 21:50:52');
INSERT INTO `tbl_empresa` VALUES (27, 4, NULL, 'UdEC3HLY', '09 - TAGUATUR TURISMO', NULL, NULL, NULL, NULL, NULL, NULL, 'TAGUATUR TURISMO', 'A', '2019-02-24 21:50:56');
INSERT INTO `tbl_empresa` VALUES (28, 4, NULL, 'zuk96FJy', '10 - DOM PEDRO LAGUNA BEACH RESORT', NULL, NULL, NULL, NULL, NULL, NULL, 'DOM PEDRO LAGUNA BEACH RESORT', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (29, 4, NULL, 'ABfrC5V7', '11 - JATIÚCA HOTEL & RESORT', NULL, NULL, NULL, NULL, NULL, NULL, 'JATIÚCA HOTEL & RESORT', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (30, 4, NULL, 'b48XP2aU', '12 - HOTEL SIBARA', NULL, NULL, NULL, NULL, NULL, NULL, 'HOTEL SIBARA', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (31, 4, NULL, '5atzdXVN', '13 - NADAI CONFORT HOTEL & SPA', NULL, NULL, NULL, NULL, NULL, NULL, 'NADAI CONFORT HOTEL & SPA', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (32, 4, NULL, '3ybzYPEt', '14 - SANTA E BELLA TURISMO RECEPTIVO', NULL, NULL, NULL, NULL, NULL, NULL, 'SANTA E BELLA TURISMO RECEPTIVO', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (33, 4, NULL, 'KTVNagqL', '15 - HOTEL PLAZA CAMBORIU', NULL, NULL, NULL, NULL, NULL, NULL, 'HOTEL PLAZA CAMBORIU', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (34, 4, NULL, 'EFK8Tqup', '16 - HOTEL SONATA DE IRACEMA', NULL, NULL, NULL, NULL, NULL, NULL, 'HOTEL SONATA DE IRACEMA', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (35, 4, NULL, 'yQcS2vbt', '17 - HOTÉIS MABU', NULL, NULL, NULL, NULL, NULL, NULL, 'HOTÉIS MABU', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (36, 4, NULL, '8pBRx9D3', '18 - PARAÍBA', NULL, NULL, NULL, NULL, NULL, NULL, 'PARAÍBA', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (37, 4, NULL, 'WKPtuipU', '19 - LUCK', NULL, NULL, NULL, NULL, NULL, NULL, 'LUCK', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (38, 4, NULL, 'TcNu9v4i', '20 - HOTEL MANAIRA', NULL, NULL, NULL, NULL, NULL, NULL, 'HOTEL MANAIRA', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (39, 4, NULL, 'e5WzpYvl', '21 - JURERÊ INTERNACIONAL', NULL, NULL, NULL, NULL, NULL, NULL, 'JURERÊ INTERNACIONAL', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (40, 4, NULL, 'DVFPsWYf', '22 - SETTE TURISMO', NULL, NULL, NULL, NULL, NULL, NULL, 'SETTE TURISMO', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (41, 4, NULL, 'BPqrkDs0', '23 - UNIDAS ALUGUEL DE CARROS', NULL, NULL, NULL, NULL, NULL, NULL, 'UNIDAS ALUGUEL DE CARROS', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (42, 4, NULL, 'aXGmBc7j', '24 - WYNDHAM GOLDEN FOZ', NULL, NULL, NULL, NULL, NULL, NULL, 'WYNDHAM GOLDEN FOZ', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (43, 4, NULL, 'vpIABwPS', '24 - HOTEL GRAN MARQUISE', NULL, NULL, NULL, NULL, NULL, NULL, 'HOTEL GRAN MARQUISE', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (44, 4, NULL, 'sQWdDF4l', '24 - BEST WESTERN PLUS COPACABANA DESING', NULL, NULL, NULL, NULL, NULL, NULL, 'BEST WESTERN PLUS COPACABANA DESING', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (45, 4, NULL, '9ktPvVXu', '24 - BEST WESTERN PREMIER ARPOADOR FASHION HOTEL', NULL, NULL, NULL, NULL, NULL, NULL, 'BEST WESTERN PREMIER ARPOADOR FASHION HOTEL', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (46, 4, NULL, 'MFf178Vq', '24 - DAYS INN LAPA', NULL, NULL, NULL, NULL, NULL, NULL, 'DAYS INN LAPA', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (47, 4, NULL, 'R3KkCru1', '25 - COSTÃO DO SANTINHO RESORT', NULL, NULL, NULL, NULL, NULL, NULL, 'COSTÃO DO SANTINHO RESORT', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (48, 4, NULL, 'RKUnlTqf', '26 - LE CANTON EMPREENDIMENTOS HOTELEIROS', NULL, NULL, NULL, NULL, NULL, NULL, 'LE CANTON EMPREENDIMENTOS HOTELEIROS', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (49, 4, NULL, 'lzmr0hC5', '27 - PONTUAL RECEPTIVO', NULL, NULL, NULL, NULL, NULL, NULL, 'PONTUAL RECEPTIVO', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (50, 4, NULL, 'YL281jKG', '28 - IBEROSTAR', NULL, NULL, NULL, NULL, NULL, NULL, 'IBEROSTAR', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (51, 4, NULL, 'GILli74H', '29 - GRAND OCA MARAGOGI RESORT', NULL, NULL, NULL, NULL, NULL, NULL, 'GRAND OCA MARAGOGI RESORT', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (52, 4, NULL, 'EgNWuIPa', '30 - HOLLIDAY INN FORTALEZA', NULL, NULL, NULL, NULL, NULL, NULL, 'HOLLIDAY INN FORTALEZA', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (53, 4, NULL, 'pBXcnJ4w', '31 - LUCK', NULL, NULL, NULL, NULL, NULL, NULL, 'LUCK', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (54, 4, NULL, 'jfIpTqW5', '31 - TAMBAQUI PRAIA HOTEL', NULL, NULL, NULL, NULL, NULL, NULL, 'TAMBAQUI PRAIA HOTEL', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (55, 4, NULL, 'l47K1I9z', '32 - RIFÓLES PRAIA HOTEL', NULL, NULL, NULL, NULL, NULL, NULL, 'RIFÓLES PRAIA HOTEL', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (56, 4, NULL, 'zdx1ff1J', '32 - PONTALMAR PRAIA', '', '', '', '', '', NULL, 'PONTALMAR PRAIA', 'A', '2019-02-24 21:49:29');
INSERT INTO `tbl_empresa` VALUES (57, 4, NULL, 'Vx71hErg', '33 - WHELTOUR', NULL, NULL, NULL, NULL, NULL, NULL, 'WHELTOUR', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (58, 4, NULL, 'RvJl0fSH', '34 - GTA - GLOBAL TRAVEL ASSISTANCE', NULL, NULL, NULL, NULL, NULL, NULL, 'GTA - GLOBAL TRAVEL ASSISTANCE', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (59, 4, NULL, 'u427plZi', '35 - GOL', NULL, NULL, NULL, NULL, NULL, NULL, 'GOL', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (60, 4, NULL, 'NcsgUGyX', '36 - D\'SINTRA', NULL, NULL, NULL, NULL, NULL, NULL, 'D\'SINTRA', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (61, 4, NULL, '1pH6sK2y', '37 - REAL CLASSIC HOTEL ARACAJU', NULL, NULL, NULL, NULL, NULL, NULL, 'REAL CLASSIC HOTEL ARACAJU', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (62, 4, NULL, 'DUEXZCut', '37 - REAL PRAIA HOTEL', NULL, NULL, NULL, NULL, NULL, NULL, 'REAL PRAIA HOTEL', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (63, 4, NULL, 'ICZy5r2x', '37 - HOTEL PARQUE DAS AGUAS', NULL, NULL, NULL, NULL, NULL, NULL, 'HOTEL PARQUE DAS AGUAS', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (64, 4, NULL, 'Nhye1sdQ', '37 - REAL CLASSIC BAHIA HOTEL', NULL, NULL, NULL, NULL, NULL, NULL, 'REAL CLASSIC BAHIA HOTEL', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (65, 4, NULL, 'u4GtLe9E', '38 - RAFAIN CENTRO HOTEL', NULL, NULL, NULL, NULL, NULL, NULL, 'RAFAIN CENTRO HOTEL', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (66, 4, NULL, 'C3nGX8z1', '38 - FOZ PLAZA', NULL, NULL, NULL, NULL, NULL, NULL, 'FOZ PLAZA', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (67, 4, NULL, 'LAlFuTcX', '38 - PIETRO ANGELO', NULL, NULL, NULL, NULL, NULL, NULL, 'PIETRO ANGELO', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (68, 4, NULL, 'YmgcWT9y', '38 - CATARATAS PARK', NULL, NULL, NULL, NULL, NULL, NULL, 'CATARATAS PARK', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (69, 4, NULL, 'aVNfxRTA', '39 - HPLUS BEACH', NULL, NULL, NULL, NULL, NULL, NULL, 'HPLUS BEACH', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (70, 4, NULL, 'rWdsL30q', '40 - SANDRIN PRAIA HOTEL', NULL, NULL, NULL, NULL, NULL, NULL, 'SANDRIN PRAIA HOTEL', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (71, 4, NULL, '2qN7B0mw', '40 - HOTEL ESTAÇÃO 101', NULL, NULL, NULL, NULL, NULL, NULL, 'HOTEL ESTAÇÃO 101', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (72, 4, NULL, 'exhBq64y', '41 - ENOTEL HOTELS & RESORTS', NULL, NULL, NULL, NULL, NULL, NULL, 'ENOTEL HOTELS & RESORTS', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (73, 4, NULL, 'N5hqRK2m', '42 - FONTUR TURISMO', NULL, NULL, NULL, NULL, NULL, NULL, 'FONTUR TURISMO', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (74, 4, NULL, '2lCuNyhx', '43 - SAN MARINO CASSINO', NULL, NULL, NULL, NULL, NULL, NULL, 'SAN MARINO CASSINO', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (75, 4, NULL, 'j2T59CGV', '44 - AZUL', NULL, NULL, NULL, NULL, NULL, NULL, 'AZUL', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (76, 4, NULL, 'erJGylg0', '45 - BONITO WAY TURISMO E EVENTOS', NULL, NULL, NULL, NULL, NULL, NULL, 'BONITO WAY TURISMO E EVENTOS', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (77, 4, NULL, 'xuvPZHNb', '46 - TCH RECEPTIVO', NULL, NULL, NULL, NULL, NULL, NULL, 'TCH RECEPTIVO', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (78, 4, NULL, '5LwG0fAM', '47 - REDE CLASS HOTEL', NULL, NULL, NULL, NULL, NULL, NULL, 'REDE CLASS HOTEL', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (79, 4, NULL, 'q5uLCpKl', '48 - HAMBURGO PALACE', NULL, NULL, NULL, NULL, NULL, NULL, 'HAMBURGO PALACE', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (80, 4, NULL, 'VCb3G1Ut', '49 - LATAM', NULL, NULL, NULL, NULL, NULL, NULL, 'LATAM', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (81, 4, NULL, '9HjdsFYG', '50 - GEKOS RECEPTIVO', NULL, NULL, NULL, NULL, NULL, NULL, 'GEKOS RECEPTIVO', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (82, 4, NULL, 'EbTauS62', '51 - PARQUE UNIPRAIAS', NULL, NULL, NULL, NULL, NULL, NULL, 'PARQUE UNIPRAIAS', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (83, 4, NULL, 'VR69EX4h', '52 - MANACA', NULL, NULL, NULL, NULL, NULL, NULL, 'MANACA', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (84, 4, NULL, 'L5sXGhlK', '52 - CONTINETAL INN', NULL, NULL, NULL, NULL, NULL, NULL, 'CONTINETAL INN', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (85, 4, NULL, 'e2ACVvgY', '53 - BETO CARRERO WORLD', NULL, NULL, NULL, NULL, NULL, NULL, 'BETO CARRERO WORLD', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (86, 4, NULL, 'pyHtiRl1', '54 - LUCK NORONHA', NULL, NULL, NULL, NULL, NULL, NULL, 'LUCK NORONHA', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (87, 4, NULL, 'iZwYu8y5', '55 - AEROLINEAS ARGENTINAS', NULL, NULL, NULL, NULL, NULL, NULL, 'AEROLINEAS ARGENTINAS', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (88, 4, NULL, 'DlHCFgZu', '56 - AVIANCA', NULL, NULL, NULL, NULL, NULL, NULL, 'AVIANCA', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (89, 4, NULL, 'LCbYqmrd', '57 - JATOBA PRAIA HOTEL', NULL, NULL, NULL, NULL, NULL, NULL, 'JATOBA PRAIA HOTEL', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (90, 4, NULL, 'xlVBXy9C', '57 - TOPTUR RECEPTIVO', NULL, NULL, NULL, NULL, NULL, NULL, 'TOPTUR RECEPTIVO', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (91, 4, NULL, 'hwdDcM1A', '58 - AQUARIOS PRAIA HOTEL', NULL, NULL, NULL, NULL, NULL, NULL, 'AQUARIOS PRAIA HOTEL', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (92, 4, NULL, 'u0GFhgBY', '58 - DEL CANTO HOTEL', NULL, NULL, NULL, NULL, NULL, NULL, 'DEL CANTO HOTEL', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (93, 4, NULL, 'Y6dfDSwH', '59 - CELI PRAIA HOTEL', NULL, NULL, NULL, NULL, NULL, NULL, 'CELI PRAIA HOTEL', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (94, 4, NULL, '9sMqczgL', '59 - XINGÓ PARQUE HOTEL', NULL, NULL, NULL, NULL, NULL, NULL, 'XINGÓ PARQUE HOTEL', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (95, 4, NULL, '3mCUBvA9', '60 - DEL MAR HOTEL', NULL, NULL, NULL, NULL, NULL, NULL, 'DEL MAR HOTEL', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (96, 4, NULL, 'MReAPNiC', '60 - CONFORT', NULL, NULL, NULL, NULL, NULL, NULL, 'CONFORT', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (97, 4, NULL, '5rYy6zXw', '61 - BELLO MARE', NULL, NULL, NULL, NULL, NULL, NULL, 'BELLO MARE', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (98, 4, NULL, 'pidZ7mn1', '61 - VILLAGE PORTO DE GALINHAS', NULL, NULL, NULL, NULL, NULL, NULL, 'VILLAGE PORTO DE GALINHAS', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (99, 4, NULL, 'cA6mur0K', '62 - INFINITY BLUE RESORT & SPA', NULL, NULL, NULL, NULL, NULL, NULL, 'INFINITY BLUE RESORT & SPA', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (100, 4, NULL, 'bTkPz6RQ', '62 - IARA BEACH', NULL, NULL, NULL, NULL, NULL, NULL, 'IARA BEACH', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (101, 4, NULL, 'UvYF3qBP', '63 - PTA OPERADORA', NULL, NULL, NULL, NULL, NULL, NULL, 'PTA OPERADORA', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (102, 4, NULL, 'IqwGN0hn', '64 - PERSONAL OPERADORA', NULL, NULL, NULL, NULL, NULL, NULL, 'PERSONAL OPERADORA', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (103, 4, NULL, '7swbQPyK', '65 - QUALITY TRAVEL', NULL, NULL, NULL, NULL, NULL, NULL, 'QUALITY TRAVEL', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (104, 4, NULL, 'YCv4d7L3', '66 - EKATOURS', NULL, NULL, NULL, NULL, NULL, NULL, 'EKATOURS', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (105, 4, NULL, 'V8Mq9Q4E', '67 - AIR CANADA', NULL, NULL, NULL, NULL, NULL, NULL, 'AIR CANADA', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (106, 4, NULL, 'HugRbD4V', '68 - SURLAND', NULL, NULL, NULL, NULL, NULL, NULL, 'SURLAND', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (107, 4, NULL, 'sz1WZaHi', '69 - SATO TOURS', NULL, NULL, NULL, NULL, NULL, NULL, 'SATO TOURS', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (108, 4, NULL, 'LiRDMcTQ', '70 - SPECIAL TOURS', NULL, NULL, NULL, NULL, NULL, NULL, 'SPECIAL TOURS', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (109, 4, NULL, 'rN6uD9wP', '71 - CITY TOURS', NULL, NULL, NULL, NULL, NULL, NULL, 'CITY TOURS', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (110, 4, NULL, 'cYCnWHIJ', '72 - RIDA INTERNACIONAL TOURISM & TRAVEL', NULL, NULL, NULL, NULL, NULL, NULL, 'RIDA INTERNACIONAL TOURISM & TRAVEL', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (111, 4, NULL, 'uv8VAXBf', '73 - ROYAL MARROC', NULL, NULL, NULL, NULL, NULL, NULL, 'ROYAL MARROC', 'A', NULL);
INSERT INTO `tbl_empresa` VALUES (112, 4, NULL, 'BdFeEtZV', '74 - VPT – VIAJES PARA TODOS', NULL, NULL, NULL, NULL, NULL, NULL, 'VPT – VIAJES PARA TODOS', 'A', NULL);

-- ----------------------------
-- Table structure for tbl_evento
-- ----------------------------
DROP TABLE IF EXISTS `tbl_evento`;
CREATE TABLE `tbl_evento`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `local` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `estatus` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'A',
  `dt_criacao` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `descricao` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `imagem_complemento` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbl_evento
-- ----------------------------
INSERT INTO `tbl_evento` VALUES (4, '6º ENCONTRO DE NEGÓCIOS NEWIT', 'SÃO PAULO', 'A', '2019-02-27 23:07:14', 'O ENCONTRO DE NEGÓCIOS NEWIT: BRASIL, AMÉRICA DO SUL & EUROPA se transformou no principal evento do trade no Rio de Janeiro.\r\n\r\nNossa feira tem o objetivo principal de reunir e promover a integração do setor, permitindo o contato direto entre os expositores e os agentes de viagens, aonde terão a oportunidade de apresentar suas marcas e oferecer novos produtos ao mercado.\r\n\r\nA primeira edição do evento ocorreu no ano de 2014, em formato de workshops, com a presença de 20 expositores e aproximadam', 'http://aevent.7giros.com.br/assets/images/planta-newit.jpg');

-- ----------------------------
-- Table structure for tbl_items
-- ----------------------------
DROP TABLE IF EXISTS `tbl_items`;
CREATE TABLE `tbl_items`  (
  `itemId` int(11) NOT NULL AUTO_INCREMENT,
  `itemHeader` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Heading',
  `itemSub` varchar(1021) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'sub heading',
  `itemDesc` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'content or description',
  `itemImage` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT 0,
  `createdBy` int(11) NOT NULL,
  `createdDtm` datetime(0) NOT NULL,
  `updatedDtm` datetime(0) NULL DEFAULT NULL,
  `updatedBy` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`itemId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbl_items
-- ----------------------------
INSERT INTO `tbl_items` VALUES (1, 'jquery.validation.js', 'Contribution towards jquery.validation.js', 'jquery.validation.js is the client side javascript validation library authored by Jörn Zaefferer hosted on github for us and we are trying to contribute to it. Working on localization now', 'validation.png', 0, 1, '2015-09-02 00:00:00', NULL, NULL);
INSERT INTO `tbl_items` VALUES (2, 'CodeIgniter User Management', 'Demo for user management system', 'This the demo of User Management System (Admin Panel) using CodeIgniter PHP MVC Framework and AdminLTE bootstrap theme. You can download the code from the repository or forked it to contribute. Usage and installation instructions are provided in ReadMe.MD', 'cias.png', 0, 1, '2015-09-02 00:00:00', NULL, NULL);

-- ----------------------------
-- Table structure for tbl_palestra
-- ----------------------------
DROP TABLE IF EXISTS `tbl_palestra`;
CREATE TABLE `tbl_palestra`  (
  `id_palestra` int(10) NOT NULL,
  `nome_palestra` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nome_palestrante` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `descricao` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `data` date NOT NULL,
  `local_palestra` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fk_evento` int(11) NOT NULL,
  `hora` time(0) NOT NULL,
  `estatus` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'A',
  `dt_criacao` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0)
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbl_palestra
-- ----------------------------
INSERT INTO `tbl_palestra` VALUES (1, 'Perú o país mais rico do mundo!', 'Elson Espinoza', NULL, '2019-03-22', '', 4, '09:00:00', 'A', '2019-02-26 21:54:48');
INSERT INTO `tbl_palestra` VALUES (2, 'Turismo de base comunitária', 'Maria dias', NULL, '2019-03-22', ' ', 4, '09:50:00', 'A', '2019-02-26 21:54:51');
INSERT INTO `tbl_palestra` VALUES (3, 'Surland: 36 anos de roteiros pela Europa, Extremo Oriente e Outras Culturas', 'Rafael Galán', NULL, '2019-03-22', ' ', 4, '10:40:00', 'A', '2019-02-26 21:54:59');
INSERT INTO `tbl_palestra` VALUES (5, 'Destino Uruguay', 'Nicolás Vidal', NULL, '2019-03-22', ' ', 4, '11:30:00', 'A', '2019-02-26 21:55:38');
INSERT INTO `tbl_palestra` VALUES (6, 'Capitólio : O mar de Minas', 'Lucas Condurú Davis', NULL, '2019-03-22', ' ', 4, '12:20:00', 'A', '2019-02-26 21:55:31');
INSERT INTO `tbl_palestra` VALUES (8, 'Argentina e suas variadas opções', 'Pablo Di Menna', NULL, '2019-03-22', ' ', 4, '13:10:00', 'A', '2019-02-26 21:55:29');
INSERT INTO `tbl_palestra` VALUES (9, 'Serra Gaúcha o Ano inteiro', 'Guilherme Ribeiro', NULL, '2019-03-22', ' ', 4, '14:00:00', 'A', '2019-02-26 21:55:25');
INSERT INTO `tbl_palestra` VALUES (10, 'Uma visão geral das opções de destinos do Chile', 'Felipe Azocar', NULL, '2019-03-22', ' ', 4, '14:50:00', 'A', '2019-02-26 21:55:20');
INSERT INTO `tbl_palestra` VALUES (11, 'Beto Carrero World – A Fantasia que te Leva', 'Renata Oliveira', NULL, '2019-03-22', ' ', 4, '15:40:00', 'A', '2019-02-26 21:55:11');
INSERT INTO `tbl_palestra` VALUES (12, 'Europa, Oriente Médio, Ásia, África, Austrália e Oceania – 4 Continentes com Special Tours', 'Alejandro Lavin', NULL, '2019-03-22', ' ', 4, '16:30:00', 'A', '2019-02-26 21:55:07');
INSERT INTO `tbl_palestra` VALUES (13, 'Paraíba, Muito Mais que Sol e Mar', 'Debora Luna', NULL, '0000-00-00', ' ', 4, '17:20:00', 'A', '2019-02-26 21:55:03');

-- ----------------------------
-- Table structure for tbl_reset_password
-- ----------------------------
DROP TABLE IF EXISTS `tbl_reset_password`;
CREATE TABLE `tbl_reset_password`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `activation_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `agent` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `client_ip` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT 0,
  `createdBy` bigint(20) NOT NULL DEFAULT 1,
  `createdDtm` datetime(0) NOT NULL,
  `updatedBy` bigint(20) NULL DEFAULT NULL,
  `updatedDtm` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for tbl_roles
-- ----------------------------
DROP TABLE IF EXISTS `tbl_roles`;
CREATE TABLE `tbl_roles`  (
  `roleId` tinyint(4) NOT NULL AUTO_INCREMENT COMMENT 'role id',
  `role` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'role text',
  PRIMARY KEY (`roleId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbl_roles
-- ----------------------------
INSERT INTO `tbl_roles` VALUES (1, 'Administrator');
INSERT INTO `tbl_roles` VALUES (2, 'Expositor');
INSERT INTO `tbl_roles` VALUES (3, 'Vistante');

-- ----------------------------
-- Table structure for tbl_users
-- ----------------------------
DROP TABLE IF EXISTS `tbl_users`;
CREATE TABLE `tbl_users`  (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `qr_code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'login email',
  `password` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'hashed login password',
  `name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'full name of user',
  `company` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `mobile` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `roleId` tinyint(4) NULL DEFAULT NULL,
  `empresaId` int(11) NULL DEFAULT NULL,
  `eventoId` int(11) NULL DEFAULT NULL,
  `isDeleted` tinyint(4) NULL DEFAULT 0,
  `createdBy` int(11) NULL DEFAULT NULL,
  `createdDtm` datetime(0) NULL DEFAULT NULL,
  `updatedBy` int(11) NULL DEFAULT NULL,
  `updatedDtm` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`userId`) USING BTREE,
  INDEX `fk_empresa`(`empresaId`) USING BTREE,
  INDEX `fk_evento`(`eventoId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 820 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbl_users
-- ----------------------------
INSERT INTO `tbl_users` VALUES (1, NULL, 'admin@gmail.com', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'Administrador', NULL, '9890098900', 1, NULL, 4, 0, 0, '2015-07-01 18:56:49', 1, '2017-06-19 09:22:53');
INSERT INTO `tbl_users` VALUES (2, NULL, 'expositor@gmail.com', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'Expositor', NULL, '9890098900', 2, 16, 4, 0, 1, '2016-12-09 17:49:56', 1, '2019-01-08 20:26:29');
INSERT INTO `tbl_users` VALUES (3, 'q1q2q3q4q5', 'visitante@gmail.com', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'Visitante', NULL, '9890098900', 3, NULL, 4, 0, 1, '2016-12-09 17:50:22', 1, '2017-06-19 09:23:21');
INSERT INTO `tbl_users` VALUES (4, 'q5q4q3q2q1', 'eddy@gmail.com', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'Eddy', NULL, '1988607536', 3, NULL, 4, 0, 1, '2019-01-09 20:31:00', 1, '2019-01-16 16:15:51');
INSERT INTO `tbl_users` VALUES (716, NULL, 'comercial@rafaincentro.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'ADILSON', 'RAFAIN CENTRO HOTEL', '45 984016894', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (717, NULL, 'adjani.ebner@satotours.eu', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'ADJANI EBNER', 'SATO TOURS', '004369919419633', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (718, NULL, 'adriana.regadas@wyndhamgramadotermas.com', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'Adriana Regadas', 'WYNDHAM GRAMADO TERMAS RESORT & SPA', '1997012975', 2, NULL, NULL, 0, NULL, NULL, 1, '2019-02-28 14:20:45');
INSERT INTO `tbl_users` VALUES (719, NULL, 'Amotta@voegol.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'ADRIANA MOTTA', 'GOL LINHAS AéREAS', '71 99136-9371', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (720, NULL, 'adriano@gekos.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'ADRIANO BRITO', 'GEKOS RECEPTIVO', '98982052929', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (721, NULL, 'a.lavin@specialtours.com', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'ALEJANDRO LAVIN', 'SPECIAL TOURS', '+34917581408', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (722, NULL, 'alessandra.navega@iberostar.com', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'ALESSANDRA NAVEGA', 'IBEROSTAR HOTELS & RESORTS', '21 97140-7452', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (723, NULL, 'alexandra.coelho@avianca.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'COELHO', 'AVIANCA', '21982732366', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (724, NULL, 'a.lopes@enotel.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'ALICE LOPES', 'ENOTEL HOTELS & RESORTS', '21993756578', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (725, NULL, 'analucia.gomes@aircanada.ca', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'ANA LUCIA GOMES', 'AIR CANADA', '21 2517-1198 ', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (726, NULL, 'anderson@gtaassist.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'ANDERSON MARQUES ', 'GTA.GLOBAL TRAVEL ASSISTANCE', '5521965565427', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (727, NULL, 'andre.kabutomori@snowland.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'KABUTOMORI', 'SNOWLAND', '11956107971', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (728, NULL, 'guilherme.ribeiro@brockerturismo.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'ANDRé KABUTOMORI', 'SNOWLAND', '54981707339', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (729, NULL, 'andre@tchreceptivo.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'ANDRÉ MATOS', 'TCH RECEPTIVO', '71999778850', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (730, NULL, 'andre.coutinho@unidas.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'ANDRE COUTINHO', 'UNIDAS ALUGUEL DE CARROS', '2196750 3957', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (731, NULL, 'comercial@hotelestacao101.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'THOMÉ', 'HOTEL ESTAÇÃO 101', '(47) 988316235', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (732, NULL, 'CAREN@GRUPONOBILE.COM.BR', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'CAREN RIBEIRO', 'REDE NOBILE & HOTEL GRAN MARQUISE', '21 994771294', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (733, NULL, 'carla@clctur.com', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'CASTRO', 'SANDRIN PRAIA HOTEL', '21986734058', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (734, NULL, 'carla@clctur.com', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'CARLA AMâNCIO', 'CLC REPRESENTAçãO DE HOTéIS', '21 98673-4058', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (735, NULL, 'carlise@brockerturismo.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'CARLISE BIANCHI', 'BROCKER TURISMO', '54981707339', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (736, NULL, 'gerencia@jatobahotel.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'HENRIQUE ', 'JATOBA PRAIA HOTEL', '079-3226-3993 / 079-99972-6120', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (737, NULL, 'cleber.gil@latam.com', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'CLEBER GIL', 'LATAM', '11986853688', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (738, NULL, 'comercial2.rj@hoteismabu.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'CRISTIANNE MAYNARD', 'HOTéIS MABU', '21982560044', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (739, NULL, 'COMERCIAL@DELMARHOTEL.COM.BR', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'CRISTIANO LUZ', 'DEL MAR HOTEL', '79999958943', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (740, NULL, 'comercial@santaebella.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'DAGMAR ZANDONADI', 'SANTA & BELLA TURISMO RECEPTIVO', '4733446792', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (741, NULL, 'd.silva@specialtours.com', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'DANIELE SILVA', 'SPECIAL TOURS', '(21) 99531 4100', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (742, NULL, 'planejamento.pbtur@gmail.com', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'DéBORA LUNA', 'EMPRESA PARAIBANA DE TURISMO', '83996825440', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (743, NULL, 'domi.costa@vpttours.com', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'DOMI COSTA BEIRO', 'VPT - VIAJES PARA TODOS', '+34 673 28 55 36', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (744, NULL, 'dgutierrez@specialtours.com', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'DUNIA GUTIERREZ', 'SPECIAL TOURS', '(11) 97474-8758', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (745, NULL, 'g.geral@realpraiahotel.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'EDIR LEITAO', 'REAL PRAIA HOTEL', '7998819403', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (746, NULL, 'eduardo@newit.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'EDUARDO BEZERRA', 'NEWIT', '21986409642', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (747, NULL, 'eduardo@newit.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'EDUARDO BEZERRA', 'NEWIT', '21986409642', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (748, NULL, 'elianecomercial@gmail.com', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'ELIANE ALMEIDA', 'REAL CLASSIC BAHIA HOTEL', '71987472922', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (749, NULL, 'info@perutravelspecials.com', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'ELSON ESPINOZA', 'PTA OPERADORA', '(51)984702115', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (750, NULL, 'vendas@fiestahotel.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'ESTRêLA LYRA', 'FIESTA BAHIA HOTEL', '(71) 3352-1479', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (751, NULL, 'diretoria@xingoparquehotel.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'EUCLIDES BUGS', 'XINGÓ PARQUE HOTEL & RESORT', '(11)999360098', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (752, NULL, 'fabianaboechat@yahoo.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'FABIANA BOECHAT', 'HOTEL DAYS INN RIO LAPA - NOBILE HOTéIS', '981418787', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (753, NULL, 'tarifas@fontur.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'FATIMA BARROS', 'FONTUR TURISMO', '(92) 98213-7777', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (754, NULL, 'felipe.azocar@ekatours.cl', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'FELIPE AZOCAR', 'EKATOURS', '56223344389', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (755, NULL, 'falonso@qualitytravel.com.ar', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'FERNANDO ALONSO', 'QUALITY TRAVEL', '541143129310', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (756, NULL, 'gerenciadevendas@rifoles.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'FERNANDO SEQUEIRA', 'RIFÓLES PRAIA HOTEL LTDA', '84 3646.5000', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (757, NULL, 'COMERCIAL@NADAICONFORTHOTEL.COM.BR', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'CHEN', 'NADAI CONFORT HOTEL & SPA', '45998311858', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (758, NULL, 'gcomercial@aquarioshotel.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'PEREIRA', 'AQUARIOS PRAIA HOTEL', '79-99964-6119', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (759, NULL, 'gnetodasilva@royalairmaroc.com', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'GIOVANNI NETO', 'ROYAL AIR MAROC', '11 95190-0018', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (760, NULL, 'gorete.mengue@terra.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'GORETE MENGUE', 'HOTEL PLAZA CAMBORIú', '4733670700', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (761, NULL, 'guilherme.ribeiro@brockerturismo.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'GUILHERME RIBEIRO', 'BROCKER TURISMO', '54981707339', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (762, NULL, 'gustavohendrick@uol.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'GUSTAVO HENDRICK', 'VIALE HOTEIS', '21991280147', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (763, NULL, 'HAYRTON@LUCKNORONHA.COM.BR', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'HAYRTON ALMEIDA', 'LUCK NORONHA', '(81) 99963-1991', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (764, NULL, 'claudiadantas_@hotmail.com', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'CLÁUDIA DANTAS', 'HOTEL PARQUE DAS ÁGUAS', '79999071153', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (765, NULL, 'vendas@wyndhambarrario.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'INGRID VIDEIRA', 'WYNDHAM RIO BARRA', '21 3139-8024', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (766, NULL, 'jaime.turismo@yahoo.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'JAIME SANTOS', 'JAIME DOS SANTOS REPRESENTAçãO HOTELEIRA E TURISMO', '21982364039', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (767, NULL, 'g.geral@realclassichotel.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'JAIRO CARVALHO', 'REAL CLASSIC HOTEL ARACAJU', '79998122625', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (768, NULL, 'jessica@maceiomarhotel.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'JéSSICA CARVALHO', 'GRUPO MME HOTéIS ', '21992324349', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (769, NULL, 'jolur@terra.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'JOSé LUIS ', 'CITY TOURS USA INC', '11 983399243', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (770, NULL, 'josi.palhete@voeazul.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'JOSI PALHETE', 'VOEAZUL', '21 98556-4426', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (771, NULL, 'walther@hoteiseresortsbrasil.com', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'WALTHER LANGE', 'INFINITY BLUE RESORT & SPA', '21996362662', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (772, NULL, 'relacionamento@bonitoway.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'JULIANA QUEIROZ', 'BONITO WAY TURISMO E EVENTOS', '6798445-1100', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (773, NULL, 'juracy@reprotel.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'JURACY BIZACHI', 'LUCK RECEPTIVO - MACEIó', '11 991257244', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (774, NULL, 'licerio@allbraziltours.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'LICéRIO SANTOS', 'ALL BRAZIL TOURS', '45991011188', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (775, NULL, 'comercial@sanmarinocassinohotel.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'LINDAMAR CAETANO', 'SAN MARINO CASSINO HOTEL LTDA', '47997111179', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (776, NULL, 'lucas@setteturismo.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'LUCAS DAVIS', 'SETTE TURISMO', '31988682772', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (777, NULL, 'vendas@vialehoteis.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'LUCIANO FERREIRA', 'VIALE HOTEIS', '45 99132 9605', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (778, NULL, 'comercial@sibaraflathotel.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'MARCELA SPAGNOL', 'HOTEL SIBARA ', '47 999665943', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (779, NULL, 'marcia.ramalho@lecanton.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'MáRCIA RAMALHO', 'LE CANTON EMPREENDIMENTOS HOTELEIROS', '21972294059', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (780, NULL, 'marcos.motta@lecanton.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'MARCOS MOTTA', 'LE CANTON EMPREENDIMENTOS HOTELEIROS', '21984635892', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (781, NULL, 'comercialrj@jurere.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'CRISTINA ALLAN', 'JIAH - JURERÊ INTERNACIONAL ADM. HOTELEIRA', '21 9 96057270', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (782, NULL, 'gerencia@taguaturturismo.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'MARIAS DIAS', 'TAGUATUR TURISMO ', '98 98126 5859', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (783, NULL, 'vendas@continentalinn.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'MARIA JOSé', 'HOTEL CONTINENTAL 4SOUL', '(45) 98412-9632', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (784, NULL, 'repcomercial.sp@dompedrolaguna.com', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'MARISA ALVES', 'DOM PEDRO LAGUNA BEACH RESORT', '11 958710012', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (785, NULL, 'martha.carneiro@lecanton.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'MARTHA MARTINS ', 'LE CANTON EMPREENDIMENTOS HOTELEIROS', '21999553529', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (786, NULL, 'fadua@holidayfortaleza.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'MAYARA CHARONE', 'HOLIDAY INN FORTALEZA', '85999965096', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (787, NULL, 'promotor2rj@gtaassist.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'MELISSA DE AMORIM MANSON FLORES', 'GTA - GLOBAL TRAVEL ASSISTANCE', '21965897528', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (788, NULL, 'michele@sonatadeiracema.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'MICHELE HONóRIO ', 'HOTEL SONATA DE IRACEMA', '85992196667', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (789, NULL, 'nalves@buziosbeachresort.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'NATERCIA ALVES', 'BUZIOS BEACH RESORT', '21964953311', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (790, NULL, 'nicolas@personaloperadora.com.uy', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'NICOLAS VIDAL', 'PERSONAL OPERADORA', '0059899596416', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (791, NULL, 'comercial@sibaraflathotel.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'OSNY MACIEL JUNIOR', 'HOTEL SIBARA ', '47 984979449', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (792, NULL, 'otavio.oliveira@aerolineas.com.ar', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'OTáVIO OLIVEIRA', 'AEROLíNEAS ARGENTINAS', '21990332225', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (793, NULL, 'patricia.cocaro@hoteljatiuca.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'PATRíCIA CóCARO', 'JATIÚCA HOTEL & RESORT', '21-96452-8989', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (794, NULL, 'rgalan@surland.com', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'RAFAEL GALAN', 'SURLAND', '0034696660407', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (795, NULL, 'rafael@pontualturismo.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'RAFAEL PONTUAL', 'PONTUAL RECEPTIVO', '81987170062', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (796, NULL, 'renata.oliveira@betocarrero.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'RENATA OLIVEIRA', 'BETO CARRERO WORLD', '21982200183', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (797, NULL, 'ricardo.delfin@iberostar.com', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'RICARDO DELFIN', 'IBEROSTAR HOTELS & RESORTS', ' 21 99964-3501', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (798, NULL, 'vendas.bwc@nobilehoteis.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'BARBOSA', 'NOBILE HOTéIS', '21976512429', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (799, NULL, 'ronaldo.barros@lecanton.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'RONALDO BARROS', 'LE CANTON EMPREENDIMENTOS HOTELEIROS', '21985476940', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (800, NULL, 'comercial1@classhotel.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'RONIVAN OLIVEIRA', 'REDE CLASS HOTEL', '3598853-4390', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (801, NULL, 'razevedocarvalho@voegol.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'ROSANA CARVALHO', 'GOL LINHAS AéREAS', '21 983067517', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (802, NULL, 'rgfernandes@voegol.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'ROSANA FERNANDES', 'GOL LINHAS AéREAS', '21 968398123', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (803, NULL, 'GERENCIACOMERCIAL@HOTELMANAIRA.COM.BR', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'ROSSANA OLIVEIRA', 'HOTEL MANAÍRA', '(83) 93326280', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (804, NULL, 'gerenciacomercial@celihotel.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'SAMANTHA FERNANDES', 'CELI HOTEL ARACAJU', '11 94456-6251', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (805, NULL, 'GERENCIACOMERCIAL@ARAMHOTEIS.COM.BR', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'SAMIRA SAMARA', 'ARAM HOTEIS', '84 99121-2014', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (806, NULL, 'sergio@toptur.com', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'SÉRGIO OLIVEIRA', 'TOPTUR RECEPTIVO', '(79) 991416060', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (807, NULL, 'Sergio.leoneti@caminhos.tur.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'SERGIO  LEONETI', 'GTA - GLOBAL TRAVEL ASSISTANCE', '2198649-0340', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (808, NULL, 'silvana.oliveira@ridaint.com', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'SILVANA DE OLIVEIRA', 'RIDA INTERNATIONAL TOURISM & TRAVEL', '21 99735 0607', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (809, NULL, 'marketing@unipraias.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'SONIA LOPES PIMENTEL', 'PARQUE UNIPRAIAS CAMBORIú', '47 3404 7600', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (810, NULL, 'comercial.maragogi@grandoca.com', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'SUSANA VILLANUEVA', 'GRAND OCA MARAGOGI RESORT', '82999843266', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (811, NULL, 'GERENCIACOMERCIAL@HOTELMANAIRA.COM.BR', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'SUZE BRITO', 'HOTEL MANAÍRA', '(83) 993326280', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (812, NULL, 'gerencia@taguaturturismo.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'MARIA DIAS', '98981265859', '98 98126 5859', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (813, NULL, 'vendas.bwa@nobilehoteis.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'OMEGNA', 'NOBILE HOTÉIS - BEST WESTERN ARPOADOR', '21 987650351', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (814, NULL, 'marketing@bonitoway.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'THIAGO AKIRA', 'BONITO WAY TURISMO E EVENTOS', '06799669-1001', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (815, NULL, 'uly.belino@lecanton.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'ULYSSEA BELINO', 'LE CANTON EMPREENDIMENTOS HOTELEIROS', '21991822333', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (816, NULL, 'vanessa.loff@costao.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'VANESSA LOFF', 'COSTÃO DO SANTINHO RESORT', '21972843456', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (817, NULL, 'comercial@hamburgopalace.com.br', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'VERA MORITZEN', 'HAMBURGO PALACE HOTEL', '47 99661 0864', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (818, NULL, 'WHELTOUR@WHELTOUR.COM.BR', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'PALHANO', 'WHELTOUR', '8432076404', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_users` VALUES (819, NULL, 'wilson.campanelli@aerolineas.com.ar', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'WILSON CAMPANELLI', 'AEROLINEAS ARGENTINAS S/A', '21990332064', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tbl_users_evento_empresa
-- ----------------------------
DROP TABLE IF EXISTS `tbl_users_evento_empresa`;
CREATE TABLE `tbl_users_evento_empresa`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuarioId` int(11) NULL DEFAULT NULL,
  `eventoId` int(11) NULL DEFAULT NULL,
  `empresaId` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `empresa_fk`(`empresaId`) USING BTREE,
  INDEX `usuario_fk`(`usuarioId`) USING BTREE,
  INDEX `evento_fk`(`eventoId`) USING BTREE,
  CONSTRAINT `empresa_fk` FOREIGN KEY (`empresaId`) REFERENCES `tbl_empresa` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `evento_fk` FOREIGN KEY (`eventoId`) REFERENCES `tbl_evento` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `usuario_fk` FOREIGN KEY (`usuarioId`) REFERENCES `tbl_users` (`userId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbl_users_evento_empresa
-- ----------------------------
INSERT INTO `tbl_users_evento_empresa` VALUES (5, 3, 4, 16);

SET FOREIGN_KEY_CHECKS = 1;
