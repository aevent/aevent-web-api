<?php
    include("application/views/includes/header.php");
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-users"></i> Evento
            <small>Adicionar Evento</small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-default" href="<?php echo base_url(); ?>events"><i class="fa fa-list-ul"></i> Listar Eventos</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Adicionar evento </h3>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <form role="form" action="" method="post" id="form_event" role="form">

                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="nome">Nome</label>
                                            <input type="text" class="form-control" name="evento_nome" id="evento_nome" maxlength="128" required>
                                            <input type="hidden" name="evento_id" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="local">Local do Evento</label>
                                            <input type="text" class="form-control"  name="evento_local" maxlength="128" required>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="box-footer">
                                <spam class="pull-right">
                                    <a href="../events" class="btn btn-default btn-simple" >Cancelar</a>
                                    <button type="submit" class="btn btn-success btn-fill" onclick="insert()">Salvar</button>
                                </spam>
                            </div>
                            <br>
                            <br>
                        </form>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<?php
include("application/views/includes/footer.php");
?>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/events.js" charset="utf-8"></script>








