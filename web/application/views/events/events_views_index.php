<?php
    include("application/views/includes/header.php");
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-users"></i> Eventos
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-success" href="<?php echo base_url(); ?>events/add"><i class="fa fa-plus"></i> Novo Evento</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Listar Eventos</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="table" class="table table-striped table-hover dataTable ">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Evento</th>
                                <th>Local</th>
                                <th>Ações</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section>

<!--    <section class="content">-->
<!--        <div class="row">-->
<!--            <div class="col-xs-12 text-right">-->
<!--                <div class="form-group">-->
<!--                    <a class="btn btn-success" href="--><?php //echo base_url(); ?><!--events/add"><i class="fa fa-plus"></i> Novo Evento</a>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="row">-->
<!--            <div class="col-xs-12">-->
<!--                <div class="box">-->
<!--                    <div class="box-header">-->
<!--                        <h3 class="box-title">Listar Eventos</h3>-->
<!--                    </div>-->
<!--                    <div class="box-body table-responsive no-padding">-->
<!--                        <table id="table" class="table table-striped table-hover dataTable dtr-inline" cellspacing="0" width="100%">-->
<!--                            <thead>-->
<!--                            <tr>-->
<!--                                <th>#</th>-->
<!--                                <th>Evento</th>-->
<!--                                <th>Local</th>-->
<!--                                <th>Ações</th>-->
<!--                            </tr>-->
<!--                            </thead>-->
<!--                            <tbody>-->
<!--                            </tbody>-->
<!--                        </table>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </section>-->
</div>
<?php
include("application/views/includes/footer.php");
?>
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="fa fa-close"></i>
                </button>
                <h4 class="modal-title">Remover</h4>
            </div>
            <div class="modal-body">
                <p>Deseja remover o registro <b class="element_remove_row"></b> ?</p>
                <p class="text-danger">Esta operação não poderá mais ser desfeita.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-simple deleteEvent" data-dismiss="modal">Deletar</button>
                <button type="button" class="btn btn-simple" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url()?>assets/datatables.net/js/jquery.dataTables.min.js"></script>

<link href="<?php echo base_url()?>assets/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />

<script src="<?php echo base_url()?>assets/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/events.js" charset="utf-8"></script>
<script>
    window.onload = function(){
        loadListing();
    }
</script>
