<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $pageTitle;?></title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- FontAwesome 4.3.0 -->
    <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url()?>assets/toastr/build/toastr.css" rel="stylesheet" >
    <style>
        .error{
            color:red;
            font-weight: normal;
        }
        body{
            background-image: url("/assets/images/bg-travel.jpg");
        }
    </style>
    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url(); ?>assets/js/jQuery-2.1.4.min.js"></script>
    <script type="text/javascript">
        var baseURL = "<?php echo base_url(); ?>";
    </script>

</head>
<body>
<div class="container" style="background-color: #000;padding: 0 0 20px 0; margin: 30px auto 0; opacity: .8; min-height: 300px">
    <div class="row">
        <div class="col-md-12">
            <div class="text-center">
                <br>
                <br>
                <br>
                <img src="<?php echo base_url(); ?>assets/images/logo-new-it-rodape.png" height="" alt="">
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="text-center">
                <br>
                <br>
                <form role="form" action="" method="post" id="form_company_lottery" role="form">
                    <input type="hidden" id="lottery" value="2">
                    <button type="submit" class="btn btn-default btn-fill" onclick="get_wins_lottery()"><i class='fa fa-check'></i>  SORTEAR</button>
                </form>

            </div>
        </div>
    </div>

</div>
<div class="container d-none" style="background-color: #fff;padding: 0 0 20px 0; margin: 0px auto; min-height: 200px; opacity: .8; color: #000; display: none">
    <div class="row">
        <div class="col-md-12">
            <div class="text-center">
                <br>
                <h1><span class="text-success">GANHADOR</span>
                    <br>
                    <br>
                    <span style="font-size:50px" id="vencedor_nome"></span>
                    <br>
                    <small><span class="text-info" id="vencedor_empresa"></span></small>
                    <br>
                    <span id="vencedor_email"></span></h1>
            </div>
        </div>
    </div>

</div>
<div class="container no-winner" style="background-color: #fff;padding: 0 0 20px 0; margin: 0px auto; min-height: 200px; opacity: .8; color: #000; display: none">
    <div class="row">
        <div class="col-md-12">
            <div class="text-center">
                <br>
                <br>
                <h2 class="text-danger" style="font-size:50px">Não teve ganhador nessa rodada!</h2>
            </div>
            <div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
<script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/dist/js/app.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/validation.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/toastr/toastr.js" type="text/javascript"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/lottery.js" charset="utf-8"></script>