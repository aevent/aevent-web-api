<?php
    include("application/views/includes/header.php");
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-ticket"></i> Sorteio
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-success" href="<?php echo base_url(); ?>lottery/add" target="_blank"><i class="fa fa-plus"></i> NOVO SORTEIO</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Listar de Participantes</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="table" class="table table-striped table-hover dataTable ">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nome</th>
                                <th>Empresa</th>
                                <th>Email</th>
                                <th class="text-center">Ações</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<?php
include("application/views/includes/footer.php");
?>
<div class="modal fade" id="modalDisableUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="fa fa-close"></i>
                </button>
                <h4 class="modal-title">Desabilitar</h4>
            </div>
            <div class="modal-body">
                <p>Deseja desabilitar o registro para votação <b class="element_remove_row text-danger"></b> ?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-simple disableUser" data-dismiss="modal">Disabilitar</button>
                <button type="button" class="btn btn-simple" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalEnableUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="fa fa-close"></i>
                </button>
                <h4 class="modal-title">Habilitar</h4>
            </div>
            <div class="modal-body">
                <p>Deseja habilitar o registro para votação <b class="element_remove_row text-success"></b> ?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-simple disableUser" data-dismiss="modal">Habilitar</button>
                <button type="button" class="btn btn-simple" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url()?>assets/datatables.net/js/jquery.dataTables.min.js"></script>

<link href="<?php echo base_url()?>assets/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />

<script src="<?php echo base_url()?>assets/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!--<script type="text/javascript" src="--><?php //echo base_url(); ?><!--assets/js/common.js" charset="utf-8"></script>-->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/lottery.js" charset="utf-8"></script>
<script>
    window.onload = function(){
        loadListing();
    }
</script>
