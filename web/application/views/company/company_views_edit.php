<?php

$empresa_id = '';
$events_id = '';
$user_id = '';
$empresa_nome = '';
$empresa_cnpj = '';
$empresa_razao = '';
$empresa_email = '';
$empresa_cep = '';
$empresa_nome = '';
$empresa_nome = '';

foreach ($get_company as $value)
{
    $empresa_id = $value->id;
    $events_id = $value->id_evento;
    $user_id = $value->id_user;
    $empresa_nome = $value->nome;
    $empresa_cnpj = $value->cnpj;
    $empresa_razao = $value->razaosocial;
    $empresa_email = $value->email;
    $empresa_cep = $value->cep;
    $empresa_cidade = $value->cidade;
    $empresa_estado = $value->estado;
}

include("application/views/includes/header.php");
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-users"></i> Empresa
            <small>Editar Empresa</small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-default" href="<?php echo base_url(); ?>company"><i class="fa fa-list-ul"></i> Listar Empresas</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><span class="text-primary">EDITAR  <?php echo $empresa_nome?></span></h3>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <form role="form" action="" method="post" id="form_company" role="form">

                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="nome">Nome do evento <span class="text-danger">*</span></label>
                                            <select class="select form-control" id="events_id" name="events_id" placeholder="Selecione Evento" required autofocus>
                                                <option value="" class="selected">SELECIONE EVENTO</option>
                                                <?php foreach ($events_get_all as $value) { ?>
                                                    <option value="<?php echo $value->id; ?>"><?php echo $value->nome;?></option>
                                                <?php } ?>
                                            </select>
                                            <script type="text/javascript">
                                                document.getElementById("events_id").value = "<?php echo $events_id?>";
                                            </script>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="nome">Nome do Expositor <span class="text-danger">*</span></label>
                                            <select class="select form-control" id="user_id" name="user_id" placeholder="Selecione Expositor">
                                                <option value="" class="selected">SELECIONE EXPOSITOR</option>
                                                <?php foreach ($users_get_all as $value) { ?>
                                                    <option value="<?php echo $value->userId; ?>"><?php echo $value->name.' - '.$value->company?></option>
                                                <?php } ?>
                                            </select>
                                            <script type="text/javascript">
                                                document.getElementById("user_id").value = "<?php echo $user_id?>";
                                            </script>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="nome">Nome empresa <span class="text-danger">*</span> </label>
                                            <input type="text" class="form-control" name="empresa_nome" id="empresa_nome" value="<?php echo $empresa_nome?>" required>
                                            <input type="hidden" name="empresa_id" id="empresa_id" value="<?php echo $empresa_id?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="local">Cnpj empresa</label>
                                            <input type="text" class="form-control"  name="empresa_cnpj" value="<?php echo $empresa_cnpj?>"  >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="local">Razão social <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control"  name="empresa_razao" value="<?php echo $empresa_razao?>"  required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="nome">Email empresa <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" name="empresa_email" value="<?php echo $empresa_email?>"  required>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="nome">Cep</label>
                                            <input type="text" class="form-control" name="empresa_cep" value="<?php echo $empresa_cep?>" >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="nome">Cidade</label>
                                            <input type="text" class="form-control" name="empresa_cidade" value="<?php echo $empresa_cidade?>" >
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="local">Estado</label>
                                            <input type="text" class="form-control"  name="empresa_estado" value="<?php echo $empresa_estado?>" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <spam class="pull-right">
                                    <a href="../company" class="btn btn-default btn-simple" >Cancelar</a>
                                    <button type="submit" class="btn btn-success btn-fill" onclick="update()">Salvar</button>
                                </spam>
                            </div>
                            <br>
                            <br>
                        </form>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<?php
include("application/views/includes/footer.php");
?>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/company.js" charset="utf-8"></script>