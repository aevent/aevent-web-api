<?php

$empresa_id = '';
$empresa_nome = '';

foreach ($get_company as $value)
{
$empresa_id = $value->id;
$empresa_nome = $value->razaosocial;
}
?>
<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $pageTitle;?></title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- FontAwesome 4.3.0 -->
    <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url()?>assets/toastr/build/toastr.css" rel="stylesheet" >
    <style>
        .error{
            color:red;
            font-weight: normal;
        }
    </style>
    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url(); ?>assets/js/jQuery-2.1.4.min.js"></script>
    <script type="text/javascript">
        var baseURL = "<?php echo base_url(); ?>";
    </script>

</head>
<body>
<div class="container" style="border:1px solid #CCCCCC; padding: 30px; margin: 30px auto">
    <div class="row">
        <div class="col-md-12">
            <div class="text-center">
                <br>
                <img src="<?php echo base_url(); ?>assets/images/logo-new-it.png" height="" alt="">
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="text-center">
                <br>
                <h2>6º ENCONTRO DE NEGÓCIOS NEWIT</h2>
                <br>
                <br>
                <h1>SORTEIO
                </h1>
            </div>
        </div>
    </div>

</div>
<div class="container" style="border:1px solid #CCCCCC; padding: 30px; margin: 30px auto">

    <div class="row">
        <div class="col-md-6">
            <div class="text-center">
                <br>
                <h1><?php echo $empresa_nome; ?> </h1>
                <br>
                <br>
                <br>
                <form role="form" action="" method="post" id="form_company_lottery" role="form">
                    <input type="hidden" id="company_id" name="company_id" value="<?php echo $empresa_id;?>">
                            <button type="submit" class="btn btn-success btn-fill" onclick="get_wins_lottery(this)">Sortear</button>
                </form>

            </div>
        </div>
        <div class="col-md-6">
            <div class="text-center d-none" style="display: none">
                <br>
                <h1>GANHADOR
                <br>
                <br>
                    <span class="text-success" id="nome_vencedor"></span>
                    <br>
                    <span class="text-success" id="email_vencedor"></span></h1>
            </div>
        </div>
    </div>

</div>

</body>
</html>
<script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/dist/js/app.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/validation.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/toastr/toastr.js" type="text/javascript"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/company.js" charset="utf-8"></script>