<?php
    include("application/views/includes/header.php");
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-users"></i> Eventos
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3><?php echo $events_get_all; ?></h3>
                        <p>EVENTOS</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-ticket"></i>
                    </div>
                    <a href="<?php echo base_url(); ?>events" class="small-box-footer"> Listar Eventos <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <a href="<?php echo base_url(); ?>company"></a>
                        <h3><?php echo $company_get_all; ?></h3>
                        <p>EMPRESAS</p>
                    </div>
                    <div class="icon">
                        <i class="icon ion-ios-briefcase"></i>
                    </div>
                    <a href="<?php echo base_url();?>company" class="small-box-footer">Listar empresas <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3><?php echo $user_get_all; ?></h3>
                        <p>USUÁRIOS</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-user"></i>
                    </div>
                    <a href="<?php echo base_url();?>userListing" class="small-box-footer">Listar usuários <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3><?php echo $event_company_get_all; ?></h3>
                        <p>EVENTO POR EMPRESA</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="<?php echo base_url();?>usuarioempresaeventoListing" class="small-box-footer">Listar evento por empres <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div><!-- ./col -->
        </div>
    </section>
</div>
<?php
include("application/views/includes/footer.php");
?>

<script src="<?php echo base_url()?>assets/datatables.net/js/jquery.dataTables.min.js"></script>

<link href="<?php echo base_url()?>assets/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />

<script src="<?php echo base_url()?>assets/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

