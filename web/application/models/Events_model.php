<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Events_model extends CI_Model
{
    var $column_order = array('nome', 'local');
    var $column_search = array('nome', 'local');
    var $order = array('nome' => 'asc');

    function __construct(){
        parent::__construct();
        $this->load->database();
    }


    private function _get_datatables_query(){
        $this->db->select('tbl_e.*');
        $this->db->from('tbl_evento AS tbl_e');
        $this->db->where('tbl_e.estatus', 'A');
        $i = 0;
        foreach ($this->column_search as $item){
            if($_POST['search']['value']){
                if($i===0){
                    $this->db->group_start();
                    $this->db->like($item, strtoupper($_POST['search']['value']));
                }
                else{
                    $this->db->or_like($item, strtoupper($_POST['search']['value']));
                }
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }

        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables(){
        $this->_get_datatables_query();
//        $this->db->from('tbl_evento as tb_e');

        if($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered(){
        $this->_get_datatables_query();
//        $this->db->from('tbl_evento as tb_e');

        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all(){
        $this->db->from('tbl_evento as tb_e');
        return $this->db->count_all_results();
    }

    //BUSCA EVENTO POR ID
    public function get_md($id = null){

        $this->db->from('tbl_evento AS tbl_e');
        $this->db->where('tbl_e.id', $id);
        $query = $this->db->get();

        return $query->result();
    }


    // INSERE EVENTO
    function insert_md($arrayUpdate = array())
    {
        $arrayUpdate = array(
            'nome' => $arrayUpdate['nome'],
            'local' => $arrayUpdate['local'],
        );

        if(!empty($arrayUpdate['nome'])){
            $query = $this->db->insert('tbl_evento', $arrayUpdate);

            if($query){
                return true;
            }
        }
        return false;
    }


    // ATUALIZA EVENTO
    function update_md($arrayUpdate = array())
    {
        $id = $arrayUpdate['id'];
        $arrayUpdate = array(
            'nome' => $arrayUpdate['nome'],
            'local' => $arrayUpdate['local'],
        );

        if(!empty($id) && !empty($arrayUpdate['nome'])){
            $this->db->where('id', $id);
            $query = $this->db->update('tbl_evento', $arrayUpdate);

            if($query){
                return true;
            }
        }
        return false;
    }


    // ATUALIZA EVENTO
    function remove_md($arrayUpdate = array())
    {
        $id = $arrayUpdate['id'];
        $arrayUpdate = array(
            'estatus' => $arrayUpdate['estatus'],
        );

        if(!empty($id)){
            $this->db->where('id', $id);
            $query = $this->db->update('tbl_evento', $arrayUpdate);

            if($query){
                return true;
            }
        }
        return false;
    }









}

  