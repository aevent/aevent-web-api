<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Lottery_model extends CI_Model
{
    var $column_order = array('name', 'local');
    var $column_search = array('name', 'local');
    var $order = array('name' => 'asc');

    function __construct(){
        parent::__construct();
        $this->load->database();
    }


    private function _get_datatables_query(){

        $this->db->select('tbl_u.*, tbl_e.*');
        $this->db->from('tbl_evento AS tbl_e');
        $this->db->join('tbl_users AS tbl_u', 'tbl_u.eventoId = tbl_e.id');
        $this->db->where('tbl_u.roleId', '3');

        $i = 0;
        foreach ($this->column_search as $item){
            if($_POST['search']['value']){
                if($i===0){
                    $this->db->group_start();
                    $this->db->like($item, strtoupper($_POST['search']['value']));
                }
                else{
                    $this->db->or_like($item, strtoupper($_POST['search']['value']));
                }
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }

        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables(){
        $this->_get_datatables_query();
//        $this->db->from('tbl_evento as tb_e');

        if($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered(){
        $this->_get_datatables_query();
//        $this->db->from('tbl_evento as tb_e');

        $query = $this->db->get();
        return $query->num_rows();
    }
    public function count_all(){
        $this->db->from('tbl_users as tb_u');
        return $this->db->count_all_results();
    }

    // DISABILITA USUÁRIO PARA VOTAÇÃO
    function disable_md($id = null)
    {
        $arrayUpdate = array(
            'lottery' => 'n',
        );

        $this->db->where('tbl_users.userId', $id);
        $query = $this->db->update('tbl_users', $arrayUpdate);

        if($query){
            return true;
        }
        return false;
    }
    // HABILITA USUÁRIO PARA VOTAÇÃO
    function enable_md($id = null)
    {
        $arrayUpdate = array(
            'lottery' => 's',
        );

        $this->db->where('tbl_users.userId', $id);
        $query = $this->db->update('tbl_users', $arrayUpdate);

        if($query){
            return true;
        }
        return false;
    }

    // SORTEIA UM VENCEDOR POR EMPRESA
    public function get_md_wins($id = null){
        $this->db->select('ue.*, user.userId, user.name, user.company, user.email, user.lottery');
        $this->db->from('tbl_users_evento_empresa AS ue');
        $this->db->join('tbl_users AS user', 'user.userId = ue.usuarioId');
        $this->db->where('user.lottery', 's');
        $this->db->order_by('ue.id','RANDOM');
        $this->db->limit(1);
        $query = $this->db->get();

        return $query->result();
    }


//    private function _generate_qr_code($size = 8){
//        $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuwxyz0123456789";
//        $randomString = '';
//        for($i = 0; $i < $size; $i = $i+1){
//            $randomString .= $chars[mt_rand(0,60)];
//        }
//        return $randomString;
//    }
//
//    // BUSCA EMPRESA POR ID
//    public function get_md($id = null){
//        $this->db->from('tbl_empresa AS tbl_e');
//        $this->db->where('tbl_e.id', $id);
//        $query = $this->db->get();
//
//        return $query->result();
//    }
//
//    // SORTEIA UM VENCEDOR POR EMPRESA
//    public function get_md_wins($id = null){
//        $this->db->select('ue.*, user.userId, user.name, user.email');
//        $this->db->from('tbl_users_evento_empresa AS ue');
//        $this->db->join('tbl_users AS user', 'user.userId = ue.usuarioId');
//        $this->db->where('ue.empresaId', $id);
//        $this->db->order_by('ue.id','RANDOM');
//        $this->db->limit(1);
//        $query = $this->db->get();
//
//        return $query->result();
//    }
//
//    // RETURNA LISTAGEM DE EVENTOS
//    public function event_get_all_md(){
//
//        $this->db->from('tbl_evento AS tbl_e');
//        $this->db->where('tbl_e.estatus', 'A');
//        $this->db->order_by('nome');
//        $query = $this->db->get();
//
//        return $query->result();
//    }
//    // RETURNA LISTAGEM DE USUÁRIO
//    public function user_get_all_md(){
//
//        $this->db->from('tbl_users AS tbl_u');
//        $this->db->where('tbl_u.roleid', 2);
//        $this->db->order_by('name');
//        $query = $this->db->get();
//
//        return $query->result();
//    }
//
//
//    // INSERE EMPRESA
//    function insert_md($arrayUpdateMD = array())
//    {
//        $qr_code = $this->_generate_qr_code();
//        if(empty($arrayUpdateMD['id_user'])){
//            $arrayUpdate = array(
//                'id_evento' => $arrayUpdateMD['id_evento'],
//                'qr_code' => $qr_code,
//                'nome' => $arrayUpdateMD['nome'],
//                'cnpj' => $arrayUpdateMD['cnpj'],
//                'razaosocial' => $arrayUpdateMD['razaosocial'],
//                'cep' => $arrayUpdateMD['cep'],
//                'cidade' => $arrayUpdateMD['cidade'],
//                'estado' => $arrayUpdateMD['estado'],
//                'email' => $arrayUpdateMD['email']
//            );
//        }else{
//            $arrayUpdate = array(
//                'id_evento' => $arrayUpdateMD['id_evento'],
//                'id_user' => $arrayUpdateMD['id_user'],
//                'qr_code' => $qr_code,
//                'nome' => $arrayUpdateMD['nome'],
//                'cnpj' => $arrayUpdateMD['cnpj'],
//                'razaosocial' => $arrayUpdateMD['razaosocial'],
//                'cep' => $arrayUpdateMD['cep'],
//                'cidade' => $arrayUpdateMD['cidade'],
//                'estado' => $arrayUpdateMD['estado'],
//                'email' => $arrayUpdateMD['email']
//            );
//        }
//
//        if(!empty($arrayUpdate['nome'])){
//            $this->db->db_debug = false;
//            $query = $this->db->insert('tbl_empresa', $arrayUpdate);
//
//            if($query){
//                return true;
//            }else{
//                $error = $this->db->error();
//                return $error;
//            }
//        }
//        return false;
//    }
//
//
//    // ATUALIZA EMPRESA
//    function update_md($arrayUpdateMD = array())
//    {
//        $qr_code = $this->_generate_qr_code();
//        $id = $arrayUpdateMD['id'];
//
//        if(empty($arrayUpdateMD['id_user'])){
//            $arrayUpdate = array(
//                'id_evento' => $arrayUpdateMD['id_evento'],
//                'qr_code' => $qr_code,
//                'nome' => $arrayUpdateMD['nome'],
//                'cnpj' => $arrayUpdateMD['cnpj'],
//                'razaosocial' => $arrayUpdateMD['razaosocial'],
//                'cep' => $arrayUpdateMD['cep'],
//                'cidade' => $arrayUpdateMD['cidade'],
//                'estado' => $arrayUpdateMD['estado'],
//                'email' => $arrayUpdateMD['email']
//            );
//        }else{
//            $arrayUpdate = array(
//                'id_evento' => $arrayUpdateMD['id_evento'],
//                'id_user' => $arrayUpdateMD['id_user'],
//                'qr_code' => $qr_code,
//                'nome' => $arrayUpdateMD['nome'],
//                'cnpj' => $arrayUpdateMD['cnpj'],
//                'razaosocial' => $arrayUpdateMD['razaosocial'],
//                'cep' => $arrayUpdateMD['cep'],
//                'cidade' => $arrayUpdateMD['cidade'],
//                'estado' => $arrayUpdateMD['estado'],
//                'email' => $arrayUpdateMD['email']
//            );
//        }
//
//        if(!empty($id)){
//            $this->db->db_debug = false;
//            $this->db->where('id', $id);
//            $query = $this->db->update('tbl_empresa', $arrayUpdate);
//
//            if($query){
//                return true;
//            }else{
//                $error = $this->db->error();
//                return $error;
//            }
//        }
//        return false;
//    }
//
//
//    // ATUALIZA EMPRESA
//    function remove_md($arrayUpdate = array())
//    {
//        $id = $arrayUpdate['id'];
//        $arrayUpdate = array(
//            'estatus' => $arrayUpdate['estatus'],
//        );
//
//        if(!empty($id)){
//            $this->db->where('id', $id);
//            $query = $this->db->update('tbl_empresa', $arrayUpdate);
//
//            if($query){
//                return true;
//            }
//        }
//        return false;
//    }

}

  