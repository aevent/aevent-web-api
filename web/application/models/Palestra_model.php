<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Palestra_model extends CI_Model
{        
    /**
     * This function is used to get the full list of palestras
     * @return array $result : This is result
     */
    function palestraListingAll()
    {
        $this->db->select('p.id_palestra as id, p.nome_palestra as nomePalestra, p.nome_palestrante as nomePalestrante, p.local_palestra as localPalestra, p.descricao, DATE_FORMAT(p.data, "%d/%m/%Y") as data, TIME_FORMAT(p.hora, "%H:%i") as hora');
        $this->db->from('tbl_palestra as p');
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }   

  
}  
	
