<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class User_Empresa_Evento_Model extends CI_Model
{
    /**
     * This function is used to get the full list of relations
     * @return array $result : This is result
     */
    function userempresaeventoListingAll()
    {
        $this->db->select('*');
        $this->db->from('tbl_users');       
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }

    function usuarioempresaeventoListarCount($searchText = '')
    {
        $this->db->select('uee.id, uee.usuarioId, uee.eventoId, uee.empresaId, u.name, e.nome, ev.nome');
        $this->db->from('tbl_users_evento_empresa as uee');   
        $this->db->join('tbl_users as u', 'uee.usuarioId = u.userId');
        $this->db->join('tbl_empresa as e', 'uee.empresaId = e.id');  
        $this->db->join('tbl_evento as ev', 'uee.eventoId = ev.id');

        if(!empty($searchText)) {
            $likeCriteria = "(                  
                    BaseTbl.id LIKE '%".$searchText."%'
                    OR BaseTbl.usuarioId LIKE '%".$searchText."%'
                    OR BaseTbl.eventoId LIKE '%".$searchText."%'
                    OR BaseTbl.empresaId LIKE '%".$searchText."%'                                
                )";
                $this->db->where($likeCriteria);
            }
       
        $query = $this->db->get();        
        return count($query->result());
    }

    function usuarioempresaeventoListing($searchText = '', $page, $segment)
    {
//        $this->db->select('tbl_users*, tbl_users_evento_empresa.*, tbl_users_evento_empresa.*');
        $this->db->select('tbl_users.userId, tbl_users.name as username, tbl_empresa.id, tbl_empresa.razaosocial, tbl_users_evento_empresa.*');

        $this->db->from('tbl_users_evento_empresa ');

        $this->db->join('tbl_users','tbl_users_evento_empresa.usuarioId = tbl_users.userId','left');
        $this->db->join('tbl_empresa','tbl_users_evento_empresa.empresaId = tbl_empresa.id','left');  
        $this->db->join('tbl_evento','tbl_users_evento_empresa.eventoId = tbl_evento.id','left');
        $this->db->order_by('tbl_empresa.razaosocial');
        
        if(!empty($searchText)) {
            $likeCriteria = "(                  
                    BaseTbl.id LIKE '%".$searchText."%'
                    OR BaseTbl.usuarioId LIKE '%".$searchText."%'
                    OR BaseTbl.eventoId LIKE '%".$searchText."%'
                    OR BaseTbl.empresaId LIKE '%".$searchText."%'                                
                )";
                $this->db->where($likeCriteria);
            }
       
        $query = $this->db->get();
        
        $result = $query->result();

        return $result;
    } 

    function inserir($Info)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_users_evento_empresa', $Info);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }

}