<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends CI_Model
{

    //RETORNA QUANTIDADE DE EVENTOS CADASTRADOS
    public function event_get_all_md(){

        $this->db->from('tbl_evento AS tbl_e');
        $this->db->where('tbl_e.estatus', 'A');
        $query = $this->db->get();

        return $query->num_rows();
    }

    //RETORNA QUANTIDADE DE EMPRESAS CADASTRADOS
    public function company_get_all_md(){

        $this->db->from('tbl_empresa AS tbl_e');
        $this->db->where('tbl_e.estatus', 'A');
        $query = $this->db->get();

        return $query->num_rows();
    }


    //RETORNA QUANTIDADE DE USUÁRIOS CADASTRADOS
    public function user_get_all_md(){

        $this->db->from('tbl_users');
//        $this->db->where('tbl_e.estatus', 'A');
        $query = $this->db->get();

        return $query->num_rows();
    }


    //RETORNA QUANTIDADE DE EVENTO POR EMPRESA CADASTRADOS
    public function event_company_get_all_md(){

        $this->db->from('tbl_evento AS tbl_e');
        $this->db->where('tbl_e.estatus', 'A');
        $query = $this->db->get();

        return $query->num_rows();
    }

}

  