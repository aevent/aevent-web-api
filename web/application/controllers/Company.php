<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';


class Company extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Company_model', 'company');
        $this->isLoggedIn();
        $this->global['controlName'] = 'Company';
        $this->load->library('form_validation', 'form_validation');
    }
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->global['pageTitle'] = '7Giros - Listar Empresas';
            $this->load->view('company/company_views_index', $this->global);
        }


    }

    // CRIA LISTAGEM E BUSCA EMPRESA
    public function ajax_list(){

        $list = $this->company->get_datatables();
        $data = array();

        foreach ($list as $value) {
            $row = array();
            $row[] = "<i class='text-success fa fa-check'></i>";
            $row[] = strtoupper($value->evento);
            $row[] = strtoupper($value->nome);
            $row[] = strtoupper($value->cnpj);
            $row[] = strtoupper($value->user_name);
            $row[] = strtoupper($value->email);
//            $row[] = "<td class='text-right'><a href='/company/lottery/".$value->id."' class='btn btn-simple btn-default btn-icon'><i class='fa fa-money'></i></a> <a href='/company/edit/".$value->id."' class='btn btn-simple btn-primary btn-icon'><i class='fa fa-edit'></i></a> <button data-id='".$value->id."' data-nome='".$value->nome."' onclick='remove(this)' data-toggle='modal' data-target='#modalDelete' class='btn btn-simple btn-danger btn-icon'><i class='fa fa-times'></i></button></td>";
            $row[] = "<td class='text-right'> <a href='/company/edit/".$value->id."' class='btn btn-simple btn-primary btn-icon'><i class='fa fa-edit'></i></a> <button data-id='".$value->id."' data-nome='".$value->nome."' onclick='remove(this)' data-toggle='modal' data-target='#modalDelete' class='btn btn-simple btn-danger btn-icon'><i class='fa fa-times'></i></button></td>";

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->company->count_all(),
            "recordsFiltered" => $this->company->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    // TELA ADICIONAR EMPRESA
    public function add()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->global['pageTitle'] = '7Giros - Adicionar Empresa';

            $this->global['events_get_all'] = $this->company->event_get_all_md();
            $this->global['users_get_all'] = $this->company->user_get_all_md();
            $this->load->view('company/company_views_add', $this->global);
        }
    }
    //  INSERIR EMPRESA
    public function insert(){

        $encoding = 'UTF-8';
        $arrayForm = array(
            'id_evento' => $this->input->post('events_id'),
            'id_user' => $this->input->post('user_id'),
            'nome' => mb_convert_case($this->input->post('empresa_nome'), MB_CASE_UPPER, $encoding),
            'cnpj' => $this->input->post('empresa_cnpj'),
            'razaosocial' => mb_convert_case($this->input->post('empresa_razao'), MB_CASE_UPPER, $encoding),
            'cep' => $this->input->post('empresa_cep'),
            'cidade' => mb_convert_case($this->input->post('empresa_cidade'), MB_CASE_UPPER, $encoding),
            'estado' => mb_convert_case($this->input->post('empresa_estado'), MB_CASE_UPPER, $encoding),
            'email' => mb_convert_case($this->input->post('empresa_email'), MB_CASE_UPPER, $encoding)
        );

        if(!empty($arrayForm['nome']) && !empty($arrayForm['id_evento'])){
            $result = $this->company->insert_md($arrayForm);
            echo json_encode($result);
        }
    }

    public function generate_qr_code($sizeini = 8, $sizefin = 8, $cont = 10){
        $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuwxyz0123456789";
        $aleatorio = rand($sizeini, $sizefin);

        for($a = 1; $a < $cont; $a = $a+1){
            $valor = substr(str_shuffle($chars), 0, $aleatorio);
            echo "<table><tr><td>".$valor."</td></tr></table>";
        }
    }


    // TELA EDITAR EMPRESA
    function edit($id = null)
    {
        if($this->isAdmin() == true || $id == 1)
        {
            $this->loadThis();
        }
        else
        {
            if($id == null)
                redirect(base_url().'../events', 'refresh');

            $this->global['pageTitle'] = '7Giros - Editar Empresa';

            $this->global['events_get_all'] = $this->company->event_get_all_md();
            $this->global['users_get_all'] = $this->company->user_get_all_md();
            $this->global['get_company'] = $this->company->get_md($id);

            $this->load->view('company/company_views_edit', $this->global);
        }
    }

    //  ATUALIZAR EMPRESA
    public function update(){

        $encoding = 'UTF-8';
        $arrayForm = array(
            'id' => $this->input->post('empresa_id'),
            'id_evento' => $this->input->post('events_id'),
            'id_user' => $this->input->post('user_id'),
            'nome' => mb_convert_case($this->input->post('empresa_nome'), MB_CASE_UPPER, $encoding),
            'cnpj' => $this->input->post('empresa_cnpj'),
            'razaosocial' => mb_convert_case($this->input->post('empresa_razao'), MB_CASE_UPPER, $encoding),
            'cep' => $this->input->post('empresa_cep'),
            'cidade' => mb_convert_case($this->input->post('empresa_cidade'), MB_CASE_UPPER, $encoding),
            'estado' => mb_convert_case($this->input->post('empresa_estado'), MB_CASE_UPPER, $encoding),
            'email' => mb_convert_case($this->input->post('empresa_email'), MB_CASE_UPPER, $encoding)
        );

        if(!empty($arrayForm['nome']) && !empty($arrayForm['id_evento']) && !empty($arrayForm['id'])){
            $result = $this->company->update_md($arrayForm);
            echo json_encode($result);
        }
    }


    // TELA SORTEIO PARTICIPANTE
    public function lottery($id = null)
    {
        if($this->isAdmin() == true || $id == 1)
        {
            $this->loadThis();
        }
        else
        {
            if($id == null)
                redirect(base_url().'../company', 'refresh');

            $this->global['pageTitle'] = '7Giros - Sorteio';

            $this->global['get_company'] = $this->company->get_md($id);
            $this->load->view('company/company_views_lottery', $this->global);
        }
    }


    // SORTEIO UM PARTICIPANTE VENCEDOR
    public function lottery_get_wins($id = null)
    {
        if($this->isAdmin() == true || $id == 1)
        {
            $this->loadThis();
        }
        else
        {
            if($id == null)
                redirect(base_url().'../company', 'refresh');


            $this->global['get_company_wins'] = $this->company->get_md_wins($id);
            echo json_encode($this->global);
        }
    }

    //  ATUALIZAR EVENTO
    public function remove($id){

        $arrayForm = array(
            'id' => $id,
            'estatus' => 'I',
        );
        if(!empty($arrayForm['id'])){
            $result = $this->company->remove_md($arrayForm);
            echo json_encode($result);
        }
    }



















































    /**
     * This function is used to load the user list
     */
    function eventoListing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('evento_model');
        
            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->evento_model->eventoListarCount($searchText);

			$returns = $this->paginationCompress ( "eventoListing/",$count,5);
            
            $data['eventoRecords'] = $this->evento_model->eventoListing($searchText, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = ' Listar Evento';
            
            $this->loadViews("listar_evento", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to load the add new form
     */
    function addEvento()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('evento_model');
            $data = $this->evento_model->getEventoRoles();
            
            $this->global['pageTitle'] = 'Adicionar novo evento';

            $this->loadViews("novoEvento", $this->global, $data, NULL);
        }
    }
      
    /**
     * This function is used to add new user to the system
     */
    function addNovoEvento()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');            
            $this->form_validation->set_rules('nome','Nome','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('local','Local','trim|required|max_length[128]|xss_clean');                    
                       
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addEvento();
            }
            else
            {                
                $nome = ucwords(strtolower($this->input->post('nome')));                
                $local = $this->input->post('local'); 
                $eventoInfo = array('nome'=> $nome, 'local'=>$local);
                $this->load->model('evento_model');
                $result = $this->evento_model->addNovoEvento($eventoInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'Novo evento criado com sucesso');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Erro ao criar Evento');
                }
                
                redirect('addNovoEvento');
            }
        }
    }
    
    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editarEvento($id = NULL)
    {
        if($this->isAdmin() == TRUE || $id == 1)
        {
            $this->loadThis();
        }
        else
        {
            if($id == null)
            {
                redirect('eventoListing');
            }            
            
            $data['eventoInfo'] = $this->evento_model->getEventoInfo($id);
            
            $this->global['pageTitle'] = 'CodeInsect : Editar Evento';
            
            $this->loadViews("editEvento", $this->global, $data, NULL);
        }
    }
    
    
    /**
     * This function is used to edit the user information
     */
    function editEvento()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $id = $this->input->post('id');
            
            $this->form_validation->set_rules('nome','Nome','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('local','Local','trim|required|max_length[128]|xss_clean');          
                       
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editEvento($id);
            }
            else
            {
                $nome = ucwords(strtolower($this->input->post('nome')));
                $local = $this->input->post('local');               
                             
                $eventoInfo = array('nome'=> $nome, 'local'=>$local);
                
                if(empty($password))
                {
                    $eventoInfo = array('nome'=>$nome, 'local'=>$local);
                }
                else
                {
                    $eventoInfo = array('nome'=>ucwords($nome));
                }
                
                $result = $this->evento_model->editEvento($eventoInfo, $id);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Evento cadastrado com sucesso');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Ao Cadastrar Evento ocorreu um erro');
                }
                
                redirect('eventoListing');
            }
        }
    }


    /**
     * This function is used to delete the user using userId
     * @return boolean $result : TRUE / FALSE
     */
    function deleteEvento()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $id = $this->input->post('id');
            $eventoInfo = array('id'=>1);
            
            $result = $this->evento_model->deleteEvento($id, $eventoInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
          
    function pageNotFound()
    {
        $this->global['pageTitle'] = 'CodeInsect : 404 - Page Not Found';
        
        $this->loadViews("404", $this->global, NULL, NULL);
    }
}

?>