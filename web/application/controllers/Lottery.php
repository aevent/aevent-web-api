<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Lottery extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Lottery_model', 'lottery');
        $this->isLoggedIn();
    }

    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->global['pageTitle'] = '7Giros - Listar Participante';
            $this->load->view('lottery/lottery_views_index', $this->global);
        }

    }


    // CRIA LISTAGEM E BUSCA EMPRESA
    public function ajax_list(){

        $list = $this->lottery->get_datatables();
        $data = array();

        foreach ($list as $value) {
            $row = array();
            if($value->lottery == 's'){
                $row[] = "<i class='text-success fa fa-check'></i>";
            }else{
                $row[] = "<i class='text-danger fa fa-minus'></i>";
            }
            $row[] = strtoupper($value->name);
            $row[] = strtoupper($value->company);
            $row[] = strtoupper($value->email);
            if($value->lottery == 's'){
                $row[] = "<td class='text-right'> <button data-id='".$value->userId."' data-nome='".$value->name."' onclick='disableUser(this)' data-toggle='modal' data-target='#modalDisableUser' class='btn btn-simple btn-default btn-icon'><i class='fa fa-minus text-danger'></i></button></td>";
            }else{
                $row[] = "<td class='text-right'> <button data-id='".$value->userId."' data-nome='".$value->name."' onclick='enableUser(this)' data-toggle='modal' data-target='#modalEnableUser' class='btn btn-simple btn-default btn-icon'><i class='fa fa-plus text-success'></i></button></td>";
            }

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->lottery->count_all(),
            "recordsFiltered" => $this->lottery->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    function pageNotFound()
    {
        $this->global['pageTitle'] = 'CodeInsect : 404 - Page Not Found';
        
        $this->loadViews("404", $this->global, NULL, NULL);
    }

    // DISABILITA USUÁRIO PARA SORTEIO
    function disableUser($id = null){
        if($this->isAdmin() == true || $id == 1)
        {
            $this->loadThis();
        }
        else
        {
            if($id == null)
                redirect(base_url().'../lottery', 'refresh');
            $result = $this->lottery->disable_md($id);
            echo json_encode($result);
        }
    }

    // HABILITA USUÁRIO PARA SORTEIO
    function enableUser($id = null){
        if($this->isAdmin() == true || $id == 1)
        {
            $this->loadThis();
        }
        else
        {
            if($id == null)
                redirect(base_url().'../lottery', 'refresh');
            $result = $this->lottery->enable_md($id);
            echo json_encode($result);
        }
    }
    // TELA SORTEIO VISITANTE
    public function add()
    {
        $this->global['pageTitle'] = '7Giros - Sorteio';

        $this->load->view('lottery/lottery_views_lottery', $this->global);
    }


    // SORTEIA UM PARTICIPANTE VENCEDOR
    public function lottery_get_wins()
    {
        $this->global = $this->lottery->get_md_wins();
        echo json_encode($this->global);
    }

}

?>