<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';


class Dashboard extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Dashboard_model', 'dashboard');
        $this->isLoggedIn();
        $this->global['controlName'] = 'Dashboard';
    }
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->global['pageTitle'] = '7Giros : Dashboard';
            $this->global['events_get_all'] = $this->dashboard->event_get_all_md();
            $this->global['company_get_all'] = $this->dashboard->company_get_all_md();
            $this->global['user_get_all'] = $this->dashboard->user_get_all_md();
            $this->global['event_company_get_all'] = $this->dashboard->event_company_get_all_md();
            $this->load->view('dashboard/dashboard_views_index', $this->global);
        }
    }





















































    /**
     * This function is used to load the user list
     */
    function eventoListing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('evento_model');
        
            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->evento_model->eventoListarCount($searchText);

			$returns = $this->paginationCompress ( "eventoListing/",$count,5);
            
            $data['eventoRecords'] = $this->evento_model->eventoListing($searchText, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = ' Listar Evento';
            
            $this->loadViews("listar_evento", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to load the add new form
     */
    function addEvento()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('evento_model');
            $data = $this->evento_model->getEventoRoles();
            
            $this->global['pageTitle'] = 'Adicionar novo evento';

            $this->loadViews("novoEvento", $this->global, $data, NULL);
        }
    }
      
    /**
     * This function is used to add new user to the system
     */
    function addNovoEvento()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');            
            $this->form_validation->set_rules('nome','Nome','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('local','Local','trim|required|max_length[128]|xss_clean');                    
                       
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addEvento();
            }
            else
            {                
                $nome = ucwords(strtolower($this->input->post('nome')));                
                $local = $this->input->post('local'); 
                $eventoInfo = array('nome'=> $nome, 'local'=>$local);
                $this->load->model('evento_model');
                $result = $this->evento_model->addNovoEvento($eventoInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'Novo evento criado com sucesso');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Erro ao criar Evento');
                }
                
                redirect('addNovoEvento');
            }
        }
    }
    
    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editarEvento($id = NULL)
    {
        if($this->isAdmin() == TRUE || $id == 1)
        {
            $this->loadThis();
        }
        else
        {
            if($id == null)
            {
                redirect('eventoListing');
            }            
            
            $data['eventoInfo'] = $this->evento_model->getEventoInfo($id);
            
            $this->global['pageTitle'] = 'CodeInsect : Editar Evento';
            
            $this->loadViews("editEvento", $this->global, $data, NULL);
        }
    }
    
    
    /**
     * This function is used to edit the user information
     */
    function editEvento()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $id = $this->input->post('id');
            
            $this->form_validation->set_rules('nome','Nome','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('local','Local','trim|required|max_length[128]|xss_clean');          
                       
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editEvento($id);
            }
            else
            {
                $nome = ucwords(strtolower($this->input->post('nome')));
                $local = $this->input->post('local');               
                             
                $eventoInfo = array('nome'=> $nome, 'local'=>$local);
                
                if(empty($password))
                {
                    $eventoInfo = array('nome'=>$nome, 'local'=>$local);
                }
                else
                {
                    $eventoInfo = array('nome'=>ucwords($nome));
                }
                
                $result = $this->evento_model->editEvento($eventoInfo, $id);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Evento cadastrado com sucesso');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Ao Cadastrar Evento ocorreu um erro');
                }
                
                redirect('eventoListing');
            }
        }
    }


    /**
     * This function is used to delete the user using userId
     * @return boolean $result : TRUE / FALSE
     */
    function deleteEvento()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $id = $this->input->post('id');
            $eventoInfo = array('id'=>1);
            
            $result = $this->evento_model->deleteEvento($id, $eventoInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
          
    function pageNotFound()
    {
        $this->global['pageTitle'] = 'CodeInsect : 404 - Page Not Found';
        
        $this->loadViews("404", $this->global, NULL, NULL);
    }
}

?>