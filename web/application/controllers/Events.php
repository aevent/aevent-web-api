<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';


class Events extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Events_model', 'events');
        $this->isLoggedIn();
        $this->global['controlName'] = 'Events';
        $this->load->library('form_validation', 'form_validation');
    }
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->global['pageTitle'] = '7Giros : Listar Eventos';
            $this->load->view('events/events_views_index', $this->global);
        }


    }

    // CRIA LISTAGEM E BUSCA EVENTOS
    public function ajax_list(){

        $list = $this->events->get_datatables();
        $data = array();

        foreach ($list as $value) {
            $row = array();
            $row[] = "<i class='text-success fa fa-check'></i>";
            $row[] = strtoupper($value->nome);
            $row[] = strtoupper($value->local);
            $row[] = "<td class='text-right'><a href='/events/edit/".$value->id."' class='btn btn-simple btn-primary btn-icon'><i class='fa fa-edit'></i></a> <button data-id='".$value->id."' data-nome='".$value->nome."' onclick='remove(this)' data-toggle='modal' data-target='#modalDelete' class='btn btn-simple btn-danger btn-icon'><i class='fa fa-times'></i></button></td>";

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->events->count_all(),
            "recordsFiltered" => $this->events->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    // TELA ADICIONAR EVENTO
    public function add()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->global['pageTitle'] = '7Giros : Adicionar Evento';
            $this->load->view('events/events_views_add', $this->global);
        }


    }
    //  INSERIR EVENTO
    public function insert(){

        $encoding = 'UTF-8';
        $arrayForm = array(
            'nome' => mb_convert_case($this->input->post('evento_nome'), MB_CASE_UPPER, $encoding),
            'local' => mb_convert_case($this->input->post('evento_local'), MB_CASE_UPPER, $encoding)
        );

        if(!empty($arrayForm['nome'])){
            $result = $this->events->insert_md($arrayForm);
            echo json_encode($result);
        }
    }


    // TELA EDITAR EVENTO
    function edit($id = null)
    {
        if($this->isAdmin() == true || $id == 1)
        {
            $this->loadThis();
        }
        else
        {
            if($id == null)
                redirect(base_url().'../events', 'refresh');

            $this->global['pageTitle'] = '7Giros : Editar Evento';

            $this->global['get_event'] = $this->events->get_md($id);

            $this->load->view('events/events_views_edit', $this->global);
        }
    }

    //  ATUALIZAR EVENTO
    public function update(){

        $encoding = 'UTF-8';
        $arrayForm = array(
            'id' => $this->input->post('evento_id'),
            'nome' => mb_convert_case($this->input->post('evento_nome'), MB_CASE_UPPER, $encoding),
            'local' => mb_convert_case($this->input->post('evento_local'), MB_CASE_UPPER, $encoding)
        );
        if(!empty($arrayForm['id']) && !empty($arrayForm['nome'])){
            $result = $this->events->update_md($arrayForm);
            echo json_encode($result);
        }
    }
    //  ATUALIZAR EVENTO
    public function remove($id){

        $arrayForm = array(
            'id' => $id,
            'estatus' => 'I',
        );
        if(!empty($arrayForm['id'])){
            $result = $this->company->remove_md($arrayForm);
            echo json_encode($result);
        }
    }



















































    /**
     * This function is used to load the user list
     */
    function eventoListing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('evento_model');
        
            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->evento_model->eventoListarCount($searchText);

			$returns = $this->paginationCompress ( "eventoListing/",$count,5);
            
            $data['eventoRecords'] = $this->evento_model->eventoListing($searchText, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = ' Listar Evento';
            
            $this->loadViews("listar_evento", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to load the add new form
     */
    function addEvento()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('evento_model');
            $data = $this->evento_model->getEventoRoles();
            
            $this->global['pageTitle'] = 'Adicionar novo evento';

            $this->loadViews("novoEvento", $this->global, $data, NULL);
        }
    }
      
    /**
     * This function is used to add new user to the system
     */
    function addNovoEvento()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');            
            $this->form_validation->set_rules('nome','Nome','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('local','Local','trim|required|max_length[128]|xss_clean');                    
                       
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addEvento();
            }
            else
            {                
                $nome = ucwords(strtolower($this->input->post('nome')));                
                $local = $this->input->post('local'); 
                $eventoInfo = array('nome'=> $nome, 'local'=>$local);
                $this->load->model('evento_model');
                $result = $this->evento_model->addNovoEvento($eventoInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'Novo evento criado com sucesso');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Erro ao criar Evento');
                }
                
                redirect('addNovoEvento');
            }
        }
    }
    
    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editarEvento($id = NULL)
    {
        if($this->isAdmin() == TRUE || $id == 1)
        {
            $this->loadThis();
        }
        else
        {
            if($id == null)
            {
                redirect('eventoListing');
            }            
            
            $data['eventoInfo'] = $this->evento_model->getEventoInfo($id);
            
            $this->global['pageTitle'] = 'CodeInsect : Editar Evento';
            
            $this->loadViews("editEvento", $this->global, $data, NULL);
        }
    }
    
    
    /**
     * This function is used to edit the user information
     */
    function editEvento()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $id = $this->input->post('id');
            
            $this->form_validation->set_rules('nome','Nome','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('local','Local','trim|required|max_length[128]|xss_clean');          
                       
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editEvento($id);
            }
            else
            {
                $nome = ucwords(strtolower($this->input->post('nome')));
                $local = $this->input->post('local');               
                             
                $eventoInfo = array('nome'=> $nome, 'local'=>$local);
                
                if(empty($password))
                {
                    $eventoInfo = array('nome'=>$nome, 'local'=>$local);
                }
                else
                {
                    $eventoInfo = array('nome'=>ucwords($nome));
                }
                
                $result = $this->evento_model->editEvento($eventoInfo, $id);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Evento cadastrado com sucesso');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Ao Cadastrar Evento ocorreu um erro');
                }
                
                redirect('eventoListing');
            }
        }
    }


    /**
     * This function is used to delete the user using userId
     * @return boolean $result : TRUE / FALSE
     */
    function deleteEvento()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $id = $this->input->post('id');
            $eventoInfo = array('id'=>1);
            
            $result = $this->evento_model->deleteEvento($id, $eventoInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
          
    function pageNotFound()
    {
        $this->global['pageTitle'] = 'CodeInsect : 404 - Page Not Found';
        
        $this->loadViews("404", $this->global, NULL, NULL);
    }
}

?>