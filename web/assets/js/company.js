

var addEventoForm = $("#form_company");

var validator = addEventoForm.validate({

	rules:{
		id_evento : { required : true },
        empresa_nome :{ required : true },
	},

	messages:{
		id_evento : { required : "Campo obrigatório!" },
        empresa_nome :{ required : "Campo obrigatório!" },

	}
});


toastr.options = {
    "closeButton": true,
    "preventDuplicates": true
}

// CARREGA LISTAGEM DE FUNCIONÁRIO
function loadListing(){
    var table;
    table = $('#table').DataTable({

        "processing": true,
        "serverSide": true,
        "order": [],
        "language": {
            "url": "../../assets/datatable-language.js",
        },
        "ajax": {
            "url": "/company/ajax_list",
            "type": "POST"
        },

        "columnDefs": [{
            "targets": [ 0 ],
            "orderable": false,
        },
        ],
    });
}

// ATUALIZA EVENTO
function insert(data){
    $('#form_company').unbind().submit(function(){
        var formdados = jQuery(this).serialize();
        var events_id = $('#events_id').val();
        var empresa_nome = $('#empresa_nome').val();
        console.log(formdados);

        if (typeof empresa_nome !== 'undefined' && typeof events_id !== 'undefined'){
            toastr.info('Aguarde...');
            $.ajax({
                url : '../company/insert',
                type: "POST",
                data: formdados,
                success: function(res, call){
                    if(res == "true"){
                        $("#form_company input").val("");
                        $('#user_id').prop('selectedIndex',-1);
                        $('#events_id').prop('selectedIndex',-1);

                        toastr.success('Cadastrado com sucesso!');
                    }
                    else{
                        toastr.error('Erro ao tentar cadastrar.');
                    }
                }
            });
        }

        return false;
    });
}



// ATUALIZA EVENTO
function update(data){
    $('#form_company').unbind().submit(function(){
        var formdados = jQuery(this).serialize();
        var events_id = $('#events_id').val();
        var empresa_nome = $('#empresa_nome').val();

        if (typeof empresa_nome !== 'undefined' && typeof events_id !== 'undefined'){
            toastr.info('Aguarde...');
            $.ajax({
                url : '../update',
                type: "POST",
                data: formdados,
                success: function(res, call){
                    if(res == "true"){
                        toastr.success('Atualizado com sucesso!');
                    }
                    else{
                        toastr.error('Erro ao tentar atualizar.');
                    }
                }
            });
        }

        return false;
    });
}

// REMOVE EVENTO
function get_wins_lottery(){
    $('#form_company_lottery').unbind().submit(function(){
        var id = $('#company_id').val();

        if (typeof id !== 'undefined'){
            toastr.info('Aguarde...');
            $.ajax({
                url : '../lottery_get_wins/'+id,
                type: "GET",
                success: function(res, call){
                    var result = JSON.parse(res);
                    $.each(result, function(i, item){
                        $(item).each(function(i, result){
                            console.log(result);
                            $(".d-none").css( "display", "block");
                            $('.d-none').fadeIn();
                            $('#nome_vencedor').html(result.name);
                            $('#email_vencedor').html(result.email);
                        });
                    });
                }
            });
        }

        return false;
    });
}

// REMOVE EVENTO
function remove(data){
    var id = $(data).data('id');
    var empresa_nome = $(data).data('nome');

    $(".element_remove_row").html(empresa_nome);

    $('.deleteEvent').unbind().click(function(){
        toastr.info('Aguarde...');
        $.ajax({
            url : '../company/remove/'+id,
            type: "GET",
            success: function(res, call){
                if(res == 'true') {
                    toastr.success('Registro removido com sucesso!');
                    var tr = $(data).closest('tr');
                    tr.fadeOut(400, function(){
                        tr.remove();
                    });
                }else{
                    toastr.error('Tente novamente, caso erro persista entre em contato com o suporte!','Erro ao tentar excluir.');
                }
            }
        });
    });
}


