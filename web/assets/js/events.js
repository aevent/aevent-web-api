

var addEventoForm = $("#form_event");

var validator = addEventoForm.validate({

	rules:{
        evento_nome :{ required : true },
		evento_local : { required : true },

	},
	messages:{
        evento_nome :{ required : "Campo obrigatório!" },
		evento_local : { required : "Campo obrigatório!" },

	}
});


toastr.options = {
    "closeButton": true,
    "preventDuplicates": true
}

// CARREGA LISTAGEM DE FUNCIONÁRIO
function loadListing(){
    var table;
    table = $('#table').DataTable({

        "processing": true,
        "serverSide": true,
        "order": [],
        "language": {
            "url": "../../assets/datatable-language.js",
        },
        "ajax": {
            "url": "/events/ajax_list",
            "type": "POST"
        },

        "columnDefs": [{
            "targets": [ 0 ],
            "orderable": false,
        },
        ],
    });
}

// ATUALIZA EVENTO
function insert(data){
    $('#form_event').unbind().submit(function(){
        var formdados = jQuery(this).serialize();
        var evento_nome = $('#evento_nome').val();

        if (typeof evento_nome !== 'undefined'){
            toastr.info('Aguarde...');
            $.ajax({
                url : '../events/insert',
                type: "POST",
                data: formdados,
                success: function(res, call){
                    if(res){
                        toastr.success('Cadastrado com sucesso!');
                    }
                    else{
                        toastr.error('Erro ao tentar cadastrar.');
                    }
                }
            });
        }

        return false;
    });
}



// ATUALIZA EVENTO
function update(data){
    $('#form_event').unbind().submit(function(){
        var formdados = jQuery(this).serialize();
        var evento_nome = $('#evento_nome').val();

        if (typeof evento_nome !== 'undefined'){
            toastr.info('Aguarde...');
            $.ajax({
                url : '../update',
                type: "POST",
                data: formdados,
                success: function(res, call){
                    if(res){
                        toastr.success('Atualizado com sucesso!');
                    }
                    else{
                        toastr.error('Erro ao tentar atualizar.');
                    }
                }
            });
        }

        return false;
    });
}

// REMOVE EVENTO
function remove(data){
    var id = $(data).data('id');
    var evento_nome = $(data).data('nome');

    $(".element_remove_row").html(evento_nome);

    $('.deleteEvent').unbind().click(function(){
        toastr.info('Aguarde...');
        $.ajax({
            url : '../events/remove/'+id,
            type: "GET",
            success: function(res, call){
                if(res == 'true') {
                    toastr.success('Registro removido com sucesso!');
                    var tr = $(data).closest('tr');
                    tr.fadeOut(400, function(){
                        tr.remove();
                    });
                }else{
                    toastr.error('Tente novamente, caso erro persista entre em contato com o suporte!','Erro ao tentar excluir.');
                }
            }
        });
    });
}


