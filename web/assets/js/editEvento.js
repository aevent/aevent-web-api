
$(document).ready(function(){
	
	var editEventoForm = $("#editEvento");
	
	var validator = editUserForm.validate({
		
		rules:{
            nome :{ required : true },
            local :{ required : true }
			
		},
		messages:{
            nome :{ required : "Insira o nome do evento" },
            local :{ required : "Por favor, insira o local onde acontecerá o evento!"},					
		}
	});
});